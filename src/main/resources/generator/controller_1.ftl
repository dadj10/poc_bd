package ${controllerPackage};
<#assign typeSet = [] />
<#list fieldList as field>
<#if ! typeSet?seq_contains(field.type.simpleName)>
        <#assign typeSet = typeSet + [field.type.simpleName] />
    </#if>
</#list>

import ${facadePackage}.AbstractFacade;
import ${facadePackage}.${modelClass.simpleName}Facade;
import ${modelPackage}.${modelClass.simpleName};
<#list typeSet as type>
    <#if (['Date','String','int','Integer','double','Double','byte','byte',
        'short','short','long','long','float','Float','Boolean','Boolean']?seq_contains(type)) ==false >
import ${modelPackage}.${type};
import ${facadePackage}.${type}Facade;
    </#if>
</#list>
import javax.faces.bean.ManagedBean;
import java.io.IOException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import javax.faces.bean.SessionScoped;
import java.text.SimpleDateFormat;

@ManagedBean
@SessionScoped
public class ${modelClass.simpleName}Controller extends AbstractController<${modelClass.simpleName}>{
    
    @PersistenceContext
    private EntityManager em;
    
    @Autowired
    private transient ${modelClass.simpleName}Facade facade;
    
    <#list typeSet as type>
        <#if (['Date','String','int','Integer','double','Double','byte','byte',
            'short','short','long','long','float','Float','Boolean','Boolean']?seq_contains(type)) ==false >
    @Autowired
    private transient ${type}Facade ${type?uncap_first}Facade;
        </#if>
    </#list>
    
    @Override
    public AbstractFacade getFacade() {
        return facade;
    }
    
    @Override
    public ${modelClass.simpleName} newEntity() {
        return new ${modelClass.simpleName}();
    }
    
    public void telechargerTemplateFichierChargementEnMasse() throws IOException{
        createActionRepport(
            <#list fieldList as field>
                <#if field.type.simpleName == 'Date'>
                    "${field.name}<#if field.requis>(*)</#if> - dd/MM/yyyy HH:mm"<#sep>,</#sep>
                <#elseif ['String','int','Integer','double','Double','byte','byte',
                            'short','short','long','long','float','Float']?seq_contains(field.type.simpleName) >
                    "${field.name}<#if field.requis>(*)</#if>"<#sep>,</#sep>
                <#elseif ['Boolean','boolean']?seq_contains(field.type.simpleName) >
                    "${field.name}<#if field.requis>(*)</#if> - O/N"<#sep>,</#sep>
                <#else>
                    "ID ${field.name}<#if field.requis>(*)</#if>"<#sep>,</#sep>
                </#if>
            </#list>
        );
        closeActionRepport(false);
        download(currentReportFile, "text/csv", "Modele.csv");
    }
    public void chargementEnMasse(){
        _chargementEnMasse("${fieldList[0].name}", 0, 
            new int[]{ <#list fieldList as field><#if field.requis == false><#continue></#if>${field?index}<#sep>,</#sep></#list> }, 
            "Les champs "+
            <#list fieldList as field>
                "${field.name}<#sep>,</#sep>"+
            </#list> 
            "doivent etre reneignés"
        );
    }
    
    @Override
    protected ${modelClass.simpleName} _chargementEnMasse(int ligne, ${modelClass.simpleName} entity, String[] datas,boolean newEntty) {
        if(newEntty){
            entity = new ${modelClass.simpleName}();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        
        <#list fieldList as field>
            <#if field.type.simpleName == 'Date'>
                if(!datas[${field?index}].trim().isEmpty()){
                    try{
                        entity.set${field.name?cap_first}(sdf.parse(datas[${field?index}]));
                    }catch(Exception ex){
                        writeActionRepport(ligne+"",String.format("'%s' n'est pas une date valide",datas[${field?index}]));
                        return null;
                    }
                }
            <#elseif 
                    field.type.simpleName == 'String' ||
                    field.type.simpleName == 'int' || field.type.simpleName == 'Integer' || 
                    field.type.simpleName == 'double' || field.type.simpleName == 'Double' || 
                    field.type.simpleName == 'byte' || field.type.simpleName == 'Byte' || 
                    field.type.simpleName == 'short' || field.type.simpleName == 'Short'|| 
                    field.type.simpleName == 'long' || field.type.simpleName == 'Long' ||
                    field.type.simpleName == 'float' || field.type.simpleName == 'Float' >
                if(!datas[${field?index}].trim().isEmpty()){
                    entity.set${field.name?cap_first}(datas[${field?index}]);
                }
            <#elseif field.type.simpleName == 'boolean' || field.type.simpleName == 'Boolean' >
                if(!datas[${field?index}].trim().isEmpty()){
                    entity.set${field.name?cap_first}("O".equalsIgnoreCase(datas[${field?index}]));
                }   
            <#else>
                ${field.type.simpleName} ${field.name} = null;
                if(!datas[${field?index}].trim().isEmpty()){
                    ${field.name}= (${field.type.simpleName}) ${field.type.simpleName?uncap_first}Facade.builder().where("id",Long.parseLong(datas[${field?index}])).findOneEntity();
                    if(${field.name} == null){
                        writeActionRepport(ligne+"",String.format("'%s' n'est pas valide",datas[${field?index}]));
                        ligne++;
                        return null;
                    }else{
                        entity.set${field.name?cap_first}(${field.name});
                    }
                }
            </#if>
        </#list>
        return entity;
    }
    
}
