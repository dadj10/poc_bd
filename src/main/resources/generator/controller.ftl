package ${controllerPackage};
import ${facadePackage}.AbstractFacade;
import ${facadePackage}.${modelClass.simpleName}Facade;
import ${modelPackage}.${modelClass.simpleName};
import javax.faces.bean.ManagedBean;
import java.io.IOException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import javax.faces.bean.ViewScoped;
import java.text.SimpleDateFormat;

@ManagedBean
@ViewScoped
public class ${modelClass.simpleName}Controller extends AbstractController<${modelClass.simpleName}>{
    
    @Autowired
    private transient ${modelClass.simpleName}Facade facade;
    
    @Override
    public AbstractFacade getFacade() {
        return facade;
    }
    
    @Override
    public ${modelClass.simpleName} newEntity() {
        return new ${modelClass.simpleName}();
    }
}
