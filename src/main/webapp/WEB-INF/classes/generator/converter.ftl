package ${converterPackage};

import ${facadePackage}.AbstractFacade;
import ${facadePackage}.${modelClass.simpleName}Facade;
import ${modelPackage}.${modelClass.simpleName};
import javax.faces.convert.FacesConverter;
import org.springframework.beans.factory.annotation.Autowired;

public class ${modelClass.simpleName}Converter{
    @FacesConverter(forClass = ${modelClass.simpleName}.class)
    public static class ConverterByClass extends AbstractConverter<${modelClass.simpleName}> {
        @Autowired
        ${modelClass.simpleName}Facade facade;
        public ConverterByClass() {
            super(${modelClass.simpleName}.class);
        }

        @Override
        public AbstractFacade<${modelClass.simpleName}> getFacade() {
            return facade;
        }
    }    
    @FacesConverter(value ="${modelClass.simpleName}Converter")
    public static class ConverterByName extends AbstractConverter<${modelClass.simpleName}> {
        @Autowired
        ${modelClass.simpleName}Facade facade;

        public ConverterByName() {
            super(${modelClass.simpleName}.class);
        }

        @Override
        public AbstractFacade<${modelClass.simpleName}> getFacade() {
            return facade;
        }
    }
}
