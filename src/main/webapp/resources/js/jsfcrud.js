function handleSubmit(args, dialog) {
    var jqDialog = jQuery('#' + dialog.substring(0,dialog.length-6)+'Dlg');
    
    console.log(args);
    if (args!= null && args.validationFailed) {
        jqDialog.effect('shake', {times: 3}, 100);
    } else {
        PF(dialog).hide();
    }
}

function handleSubmitAndNotClose(args, dialog) {
    var jqDialog = jQuery('#' + dialog.substring(0,dialog.length-6)+'Dlg');
    
    console.log(args);
    if (args!= null && args.validationFailed) {
        jqDialog.effect('shake', {times: 3}, 100);
    }
};
function handleOpen(args, dialog) {
    console.log(args);
    if (!(args!= null && args.validationFailed)) {
       PF(dialog).show();
    }
}
function sprintf( format )
{
  for( var i=1; i < arguments.length; i++ ) {
    format = format.replace( /%s/, arguments[i] );
  }
  return format;
}

$(function(){
    PrimeFaces.locales['fr'] = {
        closeText: 'Fermer',
        prevText: 'Prec',
        nextText: 'Suiv',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier','Févruer','Mars','Avril','Mai','Juin',
            'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
        monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Juin',
            'Juil','Aout','sept','Oct','Nov','Dec'],
        dayNames: ['Dimanche','Lundi','mardi','Mercredi','Jeudi','Vendredi','Samedi'],
        dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
        dayNamesMin: ['D','L','Ma','Me','J','V','S'],
        weekHeader: 'Hf',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        month: 'Mois',
        week: 'Semaine',
        day: 'Jour',
        allDayText : 'Tous les jours'
    };
})