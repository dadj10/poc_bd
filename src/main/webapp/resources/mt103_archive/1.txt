13/08/20-12:07:09 R_FIN_SGCICIAB_PRINTRTGS-10247300001 1
____________________________________________________________________


      --------------------- Instance Type and Transmission -------------- 
       Original received from SWIFT
       Priority : Normal
       Message Output Reference : 1006 200813SGCICIABDXXX0701155869
       Correspondent Input Reference : 1006 200813BCAOSNDPAXXX2651301178
      --------------------------- Message Header -------------------------
       Swift Output : FIN 103  Single Customer Credt Transfer
       Sender   : BCAOSNDPXXX
                  BANQUE CENTRALE DES ETATS DE L'AFRIQUE DE L'OUEST (B.C.E.A.O.)
                  AVENUE ABDOULAYE FADIGA
                  DAKAR
       Receiver : SGCICIABXXX
                  SOCIETE GENERALE COTE D'IVOIRE
                  5-7 AVENUE JOSEPH ANOMA BP 1355
                  ABIDJAN
       MUR : K01IFTR202260002
       Banking Priority : Normal
       STI : 
       UETR : ebc1b5eb-ac6d-40bf-a937-ddeb6dd56aef
      --------------------------- Message Text ---------------------------
        20: Sender's Reference
            K01IFTR202260002
       23B: Bank Operation Code
            CRED
       23E: Instruction Code
            SDVA
       26T: Transaction Type Code
            001
       32A: Val Dte/Curr/Interbnk Settld Amt
            Date: 200813
            Currency: XOF (CFA FRANC BCEAO)
            Amount: #30,000,000.#
       50K: 066310029-28
            /BRVM BOURSE REGIONALE DES VAL
            //BRVM-BOURSE REGIONALE DES VAL
            //PLATEAU AVE LAMBLIN RUE DAUDET
            //20315569
       53A: Sender's Correspondent - Party Identifier - Identifier Code
            /D/A00030591
            ECOCCIABXXX
            ECOBANK - COTE D'IVOIRE S.A.
            IMMEUBLE ALLIANCE AVENUE TERRASSON DE FOUGERES
            ABIDJAN
       57A: Account With Institution - Party Identifier - Identifier Code
            /C/A00030081
            SGCICIABXXX
            SOCIETE GENERALE COTE D'IVOIRE
            5-7 AVENUE JOSEPH ANOMA BP 1355
            ABIDJAN
        59: Beneficiary Customer - Account - Name & Address
            /CI008 01075 007 500 003 243 96
            //BRVM
        70: Remittance Information
            ///APPROVISIONNEMENT DE COMPTE
       71A: Details of Charges
            SHA
        72: Sender to Receiver Information
            /CODTYPTR/001
      --------------------------- Message Trailer ------------------------
       {CHK:66907A410F7C}

		
13/08/20-12:07:19 R_FIN_SGCICIAB_PRINTRTGS-10247300002 1
____________________________________________________________________


      --------------------- Instance Type and Transmission -------------- 
       Original received from SWIFT
       Priority : Normal
       Message Output Reference : 1006 200813SGCICIABAXXX3666513239
       Correspondent Input Reference : 1006 200813BCAOSNDPAXXX2651301179
      --------------------------- Message Header -------------------------
       Swift Output : FIN 910  Confirmation of Credit
       Sender   : BCAOSNDPXXX
                  BANQUE CENTRALE DES ETATS DE L'AFRIQUE DE L'OUEST (B.C.E.A.O.)
                  AVENUE ABDOULAYE FADIGA
                  DAKAR
       Receiver : SGCICIABXXX
                  SOCIETE GENERALE COTE D'IVOIRE
                  5-7 AVENUE JOSEPH ANOMA BP 1355
                  ABIDJAN
       MUR : K01IFTR202260002
       Banking Priority : Normal
      --------------------------- Message Text ---------------------------
        20: Transaction Reference Number
            931992163/910
        21: Related Reference
            K01IFTR202260002
        25: Account Identification
            A00030081
       32A: Value Date, Currency Code,  Amt
            Date: 200813
            Currency: XOF (CFA FRANC BCEAO)
            Amount: #30,000,000.#
       52A: Ordering Institution - BIC
            /D/A00030591
            ECOCCIABXXX
            ECOBANK - COTE D'IVOIRE S.A.
            IMMEUBLE ALLIANCE AVENUE TERRASSON DE FOUGERES
            ABIDJAN
      --------------------------- Message Trailer ------------------------
       {CHK:F662F4C638BC}

























		
