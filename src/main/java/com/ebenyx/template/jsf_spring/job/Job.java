package com.ebenyx.template.jsf_spring.job;

import com.ebenyx.template.jsf_spring.Main;
import java.text.SimpleDateFormat;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author ebenyx
 */
@Service
public class Job {

    final static private org.slf4j.Logger logger = LoggerFactory.getLogger(Main.class);
    final static private SimpleDateFormat SDF = new SimpleDateFormat("EEE  dd MMM yyyy");

    /*
    public void _datableTask() {
        System.out.println("---- Run job.Job._datableTask()");

        Gson gson = new Gson();

        // Je recupèle la liste de bases de données
        List<Database> databases = databaseFacade.builder().findAllEntity();
        System.out.println("----- databases " + gson.toJson(databases));
        
        // Je parcours la liste des db
        for (Database db : databases) {

            // J'initialise les varibles
            String ip = null;
            String port = null;
            String libelle = null;

            String login = null;
            String pass = null;

            ip = db.getIp();
            port = db.getPort();
            libelle = db.getLibelle();

            login = db.getLogin();
            pass = db.getPass();

            // Je verifie le type de db
            if (Constantes.DB_MYSQL.equalsIgnoreCase(db.getType())) {

                try {
                    Class.forName("com.mysql.jdbc.Driver");
                    Connection con = DriverManager.getConnection("jdbc:mysql://" + ip + ":" + port + "/" + libelle + "",
                            "" + login + "", "" + pass + "");

                    Statement statement = con.createStatement();
                    System.out.println("--- statement " + statement.toString());

                    if (statement != null) {
                        db.setStatus(Constantes.STATUS_DB_ACTIF);
                    } else {
                        db.setStatus(Constantes.STATUS_DB_INACTIF);
                    }

                    con.close();
                } catch (Exception e) {
                    db.setStatus(Constantes.STATUS_DB_INACTIF);
                    e.printStackTrace();
                }

                // mis a jour du status
                databaseFacade.edit(db);
            }

            if (db.getType().equalsIgnoreCase(Constantes.DB_POSTGRES)) {

                try {
                    String url = "jdbc:postgresql://" + ip + ":" + port + "/" + libelle + "?user=" + login
                            + "&password=" + pass;
                    Connection conn = DriverManager.getConnection(url);

                    if (conn.getSchema() != null) {
                        db.setStatus(Constantes.STATUS_DB_ACTIF);
                    } else {
                        db.setStatus(Constantes.STATUS_DB_INACTIF);
                    }
                } catch (Exception e) {
                    db.setStatus(Constantes.STATUS_DB_INACTIF);
                    e.printStackTrace();
                }

                // mis a jour du status
                databaseFacade.edit(db);
            }

            if (db.getType().equalsIgnoreCase(Constantes.DB_ORACLE)) {

                try {
                    // charge la classe du pilote
                    Class.forName("oracle.jdbc.driver.OracleDriver");

                    // créer l'objet de connexion
                    Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system",
                            "oracle");

                    // créer l'objet instruction
                    Statement statement = con.createStatement();

                    if (statement != null) {
                        db.setStatus(Constantes.STATUS_DB_ACTIF);
                    } else {
                        db.setStatus(Constantes.STATUS_DB_INACTIF);
                    }

                    con.close();

                } catch (Exception e) {
                    db.setStatus(Constantes.STATUS_DB_INACTIF);
                    System.out.println(e);
                }

                // mis a jour du status
                databaseFacade.edit(db);
            }

            if (db.getType().equalsIgnoreCase(Constantes.DB_SQL_SERVER)) {

                String connectionUrl
                        = "jdbc:sqlserver://" + ip + ".database.windows.net:" + port + ";"
                        + "database=" + libelle + ";"
                        + "user=" + login + "@yourserver;"
                        + "password=" + pass + ";"
                        + "encrypt=true;"
                        + "trustServerCertificate=false;"
                        + "loginTimeout=30;";

                ResultSet resultSet = null;

                try (
                        Connection connection = DriverManager.getConnection(connectionUrl);
                        Statement statement = connection.createStatement();
                    ) {

                    if (statement != null) {
                        db.setStatus(Constantes.STATUS_DB_ACTIF);
                    } else {
                        db.setStatus(Constantes.STATUS_DB_INACTIF);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                // mis a jour du status
                databaseFacade.edit(db);
            }
        }
    }*/
}
