/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.job.mail;

import com.ebenyx.template.jsf_spring.model.Acteur;
import com.ebenyx.template.jsf_spring.model.Evenement;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * @author ebenyx
 */
public class Mail {

    private String to;
    private String from;
    private String subject;
    private String content;
    private String cc;
    private String template;
    private HashMap<String, Object> model;
    private HashMap<String, File> attachement;

    public Mail() {
        model = new HashMap<>();
        attachement = new HashMap<>();
    }

    public String getTo() {
        return to;
    }

    public Mail setTo(String to) {
        this.to = to;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public Mail setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Mail setContent(String content) {
        this.content = content;
        return this;
    }

    public HashMap<String, Object> getModel() {
        return model;
    }

    public Mail setModel(HashMap<String, Object> model) {
        this.model = model;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getTemplate() {
        return template;
    }

    public Mail setTemplate(String template) {
        this.template = template;
        return this;
    }

    public Mail setCC(String cc) {
        this.cc = cc;
        return this;
    }

    public String getCC() {
        return cc;
    }

    public Mail data(String key, Object value) {
        this.model.put(key, value);
        return this;
    }

    public Mail attachement(String name, File file) {
        this.attachement.put(name, file);
        return this;
    }

    public HashMap<String, File> getAttachement() {
        return attachement;
    }

    public static Mail createNew() {
        return new Mail();
    }

    public static Mail evenement(String to, Evenement evenement, List<Acteur> acteurList) {
        Mail mail = createNew().data(evenement);

        TreeSet<String> emailSet = new TreeSet();
        if (acteurList != null) {
            emailSet.addAll(acteurList.stream().map(a -> a.getLogin()).collect(Collectors.toList()));
        }

        mail.setTo(to);
        if (!emailSet.isEmpty()) {
            mail.setCC(String.join(",", emailSet));
        } else {
            mail.setCC(null);
        }
        mail.setTemplate("evenement.html");
        mail.setSubject(evenement.getTypeText());
        
        return mail;
    }

    public static Mail information(
            String sujet,
            String titre,
            String information,
            String email) {
        Mail mail = createNew()
                .data("titre", titre)
                .data("information", information)
                .setTo(email);
        mail.setTemplate("information.html");
        mail.setSubject("DASHBOARD EBENYX - " + sujet);
        
        return mail;
    }

    public String getFrom() {
        return this.from;
    }

    public Mail setFrom(String from) {
        this.from = from;
        return this;
    }

    public Mail data(Evenement evenement) {
        data("titre", evenement.getTypeText()).
        data("message", evenement.getLibelle());
        
        return this;
    }
}
