package com.ebenyx.template.jsf_spring;

import freemarker.template.Template;
import freemarker.template.Configuration;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

public class CodeGenerator {

    private static final String MODEL_PACKAGE = "com.ebenyx.template.jsf_spring.model";
    private static final String CONTROLLER_PACKAGE = "com.ebenyx.template.jsf_spring.controller";
    private static final String FACADE_PACKAGE = "com.ebenyx.template.jsf_spring.facade";
    private static final String CONVERTER_PACKAGE = "com.ebenyx.template.jsf_spring.converter";
    private static final Configuration freemarkerConfig = new Configuration();
    private static Template FACADE, CONTROLLER, PAGE, CONVERTER, PAGE_DIALOG;

    static {
        initTemplate();
    }

    public static void initTemplate() {
        try {
            freemarkerConfig.setDirectoryForTemplateLoading(new File("src/main/resources/generator"));
            FACADE = freemarkerConfig.getTemplate("facade.ftl");
            CONTROLLER = freemarkerConfig.getTemplate("controller.ftl");
            PAGE = freemarkerConfig.getTemplate("page.xhtml");
            PAGE_DIALOG = freemarkerConfig.getTemplate("dialog.xhtml");
            CONVERTER = freemarkerConfig.getTemplate("converter.ftl");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    public static void main(String[] args) throws Exception {
        generateAllFromModel();
        // generateAllFromModel("Test");
        /**
         * Model m = new Model("Notification"); generateConverterFromModel(m);
         * generateFacadeFromModel(m); generateControllerFromModel(m);
         */
        String[] _class = {"Moniteur","Service"};
        for (String model : _class) {
             Model m = new Model(model);
             generateFacadeFromModel(m);
             generateConverterFromModel(m);
             generateControllerFromModel(m);
             //generatePageFromModel(m);
        }
    }

    static void generateFacadeFromModel(Model model) throws Exception {
        String code = FreeMarkerTemplateUtils.processTemplateIntoString(FACADE, model);
        createSource(model.getFacadePath(), code);
    }

    static void generateConverterFromModel(Model model) throws Exception {
        String code = FreeMarkerTemplateUtils.processTemplateIntoString(CONVERTER, model);
        createSource(model.getConverterPath(), code);
    }

    static void generateControllerFromModel(Model model) throws Exception {
        String code = FreeMarkerTemplateUtils.processTemplateIntoString(CONTROLLER, model);
        createSource(model.getControlerPath(), code);
    }

    static void generatePageFromModel(Model model) throws Exception {
        String code = FreeMarkerTemplateUtils.processTemplateIntoString(PAGE, model);
        createSource(model.getWebPagePath(), code);

        code = FreeMarkerTemplateUtils.processTemplateIntoString(PAGE_DIALOG, model);
        createSource(model.getDialogWebPagePath(), code);
    }

    static void generateAllFromModel(String... models) throws Exception {
        List<String> modelList = Arrays.asList(models);
        if (modelList.isEmpty()) {
            File folderEntity = new File("src/main/java/" + MODEL_PACKAGE.replaceAll("\\.", "/"));
            System.out.println(folderEntity.getAbsolutePath());
            modelList = Arrays.asList(folderEntity.list((dir, name) -> {
                return name.endsWith(".java") && (!"BaseEntity.java".equals(name)) && (!"BaseEntity.java".equals(name))
                        && (!"Parametre.java".equals(name)) && (!"Role.java".equals(name))
                        && (!"Acteur.java".equals(name)) && (!"TraceRequest.java".equals(name));
            })).stream().map(m -> m.substring(0, m.length() - 5)).collect(Collectors.toList());
        }

        for (String model : modelList) {
            Model m = new Model(model);
            // System.out.println(String.format("<ui:include
            // src=\"/WEB-INF/param/_%s_dialog.xhtml\"
            // />",m.modelClass.getSimpleName().toLowerCase()));

            generateFacadeFromModel(m);
            generateConverterFromModel(m);
            generateControllerFromModel(m);
            generatePageFromModel(m);

            // System.out.println(String.format("<p:menuitem
            // rendered=\"#{mainController.currentActeur.can('role_acces_gestion_%s')}\"
            // value=\"Gestion des %s\" icon=\"fa fa-fw fa-cog\" update=\"mainContent\"
            // actionListener=\"#{mainController.set('page','Gestion des %s')}\"
            // />\n",m.modelClass.getSimpleName(),m.modelClass.getSimpleName(),m.modelClass.getSimpleName()));
        }
    }

    private static void createSource(String path, String code) throws Exception {
        // System.out.println("");
        // System.out.println(code);

        File file = new File(path);
        if (file.exists()) {
            System.out.println("existe deja : " + file);
            return;
        }
        System.out.println(path);
        file.createNewFile();
        FileOutputStream outputStream = new FileOutputStream(file);
        byte[] strToBytes = code.getBytes();
        outputStream.write(strToBytes);
        outputStream.close();

    }

    public static class Model {

        private String modelPackage;
        private String controllerPackage;
        private String facadePackage;
        private String converterPackage;
        private Class modelClass;
        private List<MyField> fieldList;

        public Model(String model) throws ClassNotFoundException {
            this.modelClass = Class.forName(String.format("%s.%s", MODEL_PACKAGE, model));
            this.modelPackage = MODEL_PACKAGE;
            this.controllerPackage = CONTROLLER_PACKAGE;
            this.converterPackage = CONVERTER_PACKAGE;
            this.facadePackage = FACADE_PACKAGE;
            this.facadePackage = FACADE_PACKAGE;
            this.fieldList = Arrays.stream(this.modelClass.getDeclaredFields())
                    .filter(f -> Modifier.isPrivate(f.getModifiers()) && (!Modifier.isTransient(f.getModifiers()))
                    && (!Modifier.isStatic(f.getModifiers())) && (!Modifier.isFinal(f.getModifiers())))
                    .map(f -> new MyField(f)).collect(Collectors.toList());
        }

        public String getModelPackage() {
            return modelPackage;
        }

        public String getControllerPackage() {
            return controllerPackage;
        }

        public String getFacadePackage() {
            return facadePackage;
        }

        public String getConverterPackage() {
            return converterPackage;
        }

        public Class getModelClass() {
            return modelClass;
        }

        public List<MyField> getFieldList() {
            return fieldList;
        }

        public String getControlerPath() {
            return String.format("%s/%sController.java", getFilePath(controllerPackage), modelClass.getSimpleName());
        }

        private String getWebPagePath() {
            return "src/main/webapp/WEB-INF/param/_" + modelClass.getSimpleName().toLowerCase() + ".xhtml";
        }

        private String getDialogWebPagePath() {
            return "src/main/webapp/WEB-INF/param/_" + modelClass.getSimpleName().toLowerCase() + "_dialog.xhtml";
        }

        public String getFacadePath() {
            return String.format("%s/%sFacade.java", getFilePath(facadePackage), modelClass.getSimpleName());
        }

        public String getConverterPath() {
            return String.format("%s/%sConverter.java", getFilePath(converterPackage), modelClass.getSimpleName());
        }

        private String getFilePath(String _package) {
            return "src/main/java/" + _package.replace(".", File.separator);
        }
    }

    public static class MyField {

        private Field field;
        private boolean requis;

        public MyField(Field field) {
            this.field = field;
            this.requis = Arrays.stream(field.getDeclaredAnnotations())
                    .filter(a -> (a instanceof Column && (!((Column) a).nullable()))
                    || (a instanceof JoinColumn && (!((JoinColumn) a).nullable())))
                    .count() > 0;
        }

        public Field getField() {
            return field;
        }

        public boolean isRequis() {
            return requis;
        }

        public boolean getRequis() {
            return requis;
        }

        public Type getType() {
            return field.getType();
        }

        public String getName() {
            return field.getName();
        }

        public MyField() {
        }
    }
}
