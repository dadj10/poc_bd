/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.repository;

import com.ebenyx.template.jsf_spring.model.Acteur;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author ebenyx
 */
public interface UserRepository extends CrudRepository<Acteur, Long>{}