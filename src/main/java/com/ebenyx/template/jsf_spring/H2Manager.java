/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 *
 * @author ebenyx
 */
public class H2Manager {
	public static void main(String[] args) throws Exception {
		Class.forName("org.h2.Driver");
		Connection conn = DriverManager.getConnection(JpaConfiguration.H2DB, "sa", "");
		Statement st = conn.createStatement();
		// st.executeUpdate("drop table utilisateur;");
		// st.executeUpdate("drop table role;");
		st.executeUpdate("drop table DifficulteTacheProjet");
		// st.executeUpdate("drop table planifierreunion");
		/*
		 * st.executeUpdate("drop table typedocumentprojet");
		 * st.executeUpdate("drop table histotypedocumentprojet");
		 * st.executeUpdate("drop table tacheprojet");
		 * st.executeUpdate("drop table projet");
		 */
		// st.executeUpdate("update tacheprojet set source='' where id>2");
		// st.executeUpdate("drop table parametre");
		/*
		 * st.executeUpdate("drop table impaye");
		 * st.executeUpdate("drop table affectation_acteur");
		 * st.executeUpdate("drop table affectation");
		 * st.executeUpdate("drop table acteur"); st.executeUpdate("drop table role");
		 */
		// st.executeUpdate("delete from versement");
		conn.close();
	}

}
