/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {
	private String mode;
	private String Cc;
	private String emailsender;

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getCc() {
		return Cc;
	}

	public void setCc(String Cc) {
		this.Cc = Cc;
	}

	public String getEmailsender() {
		return emailsender;
	}

	public void setEmailsender(String emailsender) {
		this.emailsender = emailsender;
	}

}
