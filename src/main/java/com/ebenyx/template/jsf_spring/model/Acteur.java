/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.model;

import com.ebenyx.template.jsf_spring.JsfUtil;
import com.ebenyx.template.jsf_spring.controller.AbstractController;
import com.ebenyx.template.jsf_spring.facade.ActeurFacade;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(
        name = "acteur",
        indexes = {
            @Index(columnList = "nom"),
            @Index(columnList = "prenoms"),
            @Index(columnList = "uuid"),}
)
public class Acteur extends BaseEntity {

    private static final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
    private static SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");

    private String motDePasse;

    @Column(unique = true)
    private String login;

    private String nom;

    private String prenoms;

    @Column
    private String contact;

    private String civilite;

    private String token;

    private String registrationId;

    @Column(unique = true)
    public String wsToken;

    @Temporal(TemporalType.TIMESTAMP)
    private Date tokenExpireDate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "user_role",
        joinColumns = @JoinColumn(name = "user_id"),
        inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private List<Role> roles;

    private String uuid;

    public Acteur() {
        this.enabled = false;
        roles = new ArrayList<>();
        updateToken();
        updateUuid();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void updateToken() {
        token = UUID.randomUUID().toString();
        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR, 48);
        tokenExpireDate = c.getTime();
    }

    public Date getTokenExpireDate() {
        return tokenExpireDate;
    }

    public void setTokenExpireDate(Date tokenExpireDate) {
        this.tokenExpireDate = tokenExpireDate;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenoms() {
        return prenoms;
    }

    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getWsToken() {
        return wsToken;
    }

    public void setWsToken(String wsToken) {
        this.wsToken = wsToken;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void generatedWsToken() throws UnsupportedEncodingException {
        wsToken = new String(
                Base64.getEncoder().encode(
                        String.format(
                                "%s;%s",
                                this.getLogin(),
                                UUID.randomUUID()
                        ).getBytes("UTF8")
                ), "UTF-8"
        );
    }

    public static Acteur admin(List<Role> roles) {
        Acteur acteur = new Acteur();
        acteur.setEnabled(true);
            acteur.setMotDePasse(ActeurFacade.getSecurePassword("1234567890"));
        acteur.setLogin("admin");
        acteur.setNom("ADMIN");
        acteur.setPrenoms("ADMIN");
        acteur.setContact("44777911");
        acteur.getRoles().addAll(roles);
        return acteur;
    }
    
    public static Acteur agent_carte(Role role) {
        Acteur acteur = new Acteur();
        acteur.setEnabled(true);
        acteur.setMotDePasse(ActeurFacade.getSecurePassword("1234567890"));
        acteur.setLogin("agentcarte");
        acteur.setNom("AGENT");
        acteur.setPrenoms("CARTE");
        acteur.setContact("00000000");
        acteur.getRoles().add(role);

        return acteur;
    }
    
    public static Acteur agent_code(Role role) {
        Acteur acteur = new Acteur();
        acteur.setEnabled(true);
        acteur.setMotDePasse(ActeurFacade.getSecurePassword("1234567890"));
        acteur.setLogin("agentcode");
        acteur.setNom("AGENT");
        acteur.setPrenoms("CODE");
        acteur.setContact("00000000");
        acteur.getRoles().add(role);

        return acteur;
    }

    public boolean can(String role) {
        boolean can = this.getRoles().stream().filter(
                r
                -> role.equals(r.getLabel())
                || "role_admin".equals(r.getLabel())
        ).count() > 0;

        if (can) {
            return true;
        }
        for (Role r : this.getRoles()) {
            if (r.getLabel().startsWith(role + "=>")) {
                String[] roleTag = r.getLabel().split("=>");
                long id = ((AbstractController) JsfUtil.getConroller(roleTag[1])).getSelected().getId();
                long idAutorise = Long.parseLong(roleTag[2]);
                return id == idAutorise;
            }
        }
        return false;
    }

    public boolean hasRole(String role) {
        return this.getRoles().stream().filter(r -> role.equals(r.getLabel())).count() > 0;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public String getLibelle() {
        return String.format("%s %s %s (%s)",
                civilite != null ? civilite : "",
                nom != null ? nom : "",
                prenoms != null ? prenoms : "",
                login != null ? login : ""
        );
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void updateUuid() {
        this.uuid = UUID.randomUUID().toString();
    }
}
