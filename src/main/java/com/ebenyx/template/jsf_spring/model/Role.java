/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import org.springframework.security.core.GrantedAuthority;

@Entity
public class Role extends BaseEntity implements GrantedAuthority {
    
    public static final String ROLE_ADMIN = "role_ADMIN";
    public static final String ROLE_AGENT_RETRAIT_CARTE = "role_AGENT_RETRAIT_CARTE";
    public static final String ROLE_AGENT_RETRAIT_CODE = "role_AGENT_RETRAIT_CODE";

    @Column(unique = true)
    private String label;

    public Role() {
    }

    public Role(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String getAuthority() {
        return getLabel();
    }

    @Override
    public int compareTo(BaseEntity o) {
        if (o == null) {
            return -1;
        }
        if (!(o instanceof Role)) {
            return -1;
        }
        return label.compareTo(((Role) o).getLabel());
    }

    public void contextRole(String controller, Long id) {
        this.label = String.format("%s=>%s=>%s", label, controller, id);
    }
}
