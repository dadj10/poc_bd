package com.ebenyx.template.jsf_spring.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author alexwilfriedo
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable, Comparable<BaseEntity> {

    private final static PrettyTime PRETTY_TIME = new PrettyTime(new Locale("fr"));

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date created;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    protected Date updated;

    @Column
    protected boolean enabled;

    @Column
    protected boolean viewed;
    
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    protected Date viewedDate;

    @JoinColumn
    @ManyToOne(fetch = FetchType.LAZY)
    private Acteur creePar;

    @JoinColumn
    @ManyToOne(fetch = FetchType.LAZY)
    private Acteur modifierPar;

    transient protected boolean checked;

    transient protected boolean ignoreUpdateCreator;

    public BaseEntity() {
        viewed = false;
        enabled = true;
    }

    public Long getId() {
        return id;
    }

    public String getIdText() {
        return id + "";
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isViewed() {
        return viewed;
    }

    public boolean getViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
        if (this.viewed && viewedDate == null) {
            viewedDate = new Date();
        }
    }

    public boolean isChecked() {
        return checked;
    }

    public boolean getChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void toogleSelect() {
        this.checked = !checked;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BaseEntity other = (BaseEntity) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    protected void onPrePersist() {
    }

    protected void onPreUpdate() {
    }

    protected void onPreSave() {
    }

    @PrePersist
    protected void _onPrePersist() {
        if (created == null) {
            created = new Date();
            updated = new Date();
        }
        onPrePersist();
        onPreSave();
    }

    @PreUpdate
    protected void _onPreUpdate() {
        updated = new Date();
        onPreUpdate();
        onPreSave();
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getTimeCreatedAgo() {
        return prettyTime(getCreated());
    }

    public String getTimeUpdatedAgo() {
        return prettyTime(getUpdated());
    }

    public String prettyTime(Date date) {
        return PRETTY_TIME.format(date);
    }

    public Date getViewedDate() {
        return viewedDate;
    }

    public void setViewedDate(Date viewedDate) {
        this.viewedDate = viewedDate;
    }

    public Acteur getCreePar() {
        return creePar;
    }

    public void setCreePar(Acteur creePar) {
        this.creePar = creePar;
        this.modifierPar = modifierPar;
        this.setCreated(new Date());
        this.setUpdated(new Date());
    }

    public Acteur getModifierPar() {
        return modifierPar;
    }

    public void setModifierPar(Acteur modifierPar) {
        this.modifierPar = modifierPar;
        this.setUpdated(new Date());
    }

    @Override
    public int compareTo(BaseEntity o) {
        if (o == null) {
            return -1;
        }
        return o.getId().compareTo(o.getId());
    }

    public boolean getEnabled() {
        return enabled;
    }

    public Acteur getCurrentActeur() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof Acteur) {
            return (Acteur) authentication.getPrincipal();
        }
        return null;
    }

    public boolean isIgnoreUpdateCreator() {
        return ignoreUpdateCreator;
    }

    public void setIgnoreUpdateCreator(boolean ignoreUpdateCreator) {
        this.ignoreUpdateCreator = ignoreUpdateCreator;
    }
}
