/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.model;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(indexes = { @Index(columnList = "type"), @Index(columnList = "level"), }, uniqueConstraints = {})
public class Evenement extends BaseEntity {
	public final static int VERBOSE = 1;
	public final static int INFO = 2;
	public final static int AVERTISSEMENT = 3;
	public final static int SERVERE = 4;

	public static final int CONNEXION = 1;
	public static final int DECONNEXION = 2;
	public static final int NAVIGATION = 3;
	public static final int NOUVELLE_ABONNEMENT = 1;

	@Lob
	private String libelle;
	private int type;
	private int level;

	private boolean notifierActeurs;
	private boolean acteursNotifies;

	public Evenement() {
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getTypeText() {
		switch (type) {
		case CONNEXION:
			return "CONNEXION";
		case DECONNEXION:
			return "DECONNEXION";
		case NAVIGATION:
			return "NAVIGATION";
		}
		return "na";
	}

	public String getLevelText() {
		switch (type) {
		case VERBOSE:
			return "BAVARD";
		case INFO:
			return "INSCRIPTION";
		case AVERTISSEMENT:
			return "AVERTISSEMENT";
		case SERVERE:
			return "SERVERE";
		}
		return "na";
	}

	public static Builder builder(int type, int level, String libelle) {
		return new Builder(type, level, libelle);
	}

	public static class Builder {
		private Acteur acteur;
		private String libelle;
		private int type;
		private int level;
		private boolean ignoreUpdateCreator;

		public Builder(int type, int level, String libelle) {
			this.libelle = libelle;
			this.type = type;
			this.level = level;
		}

		public Builder setLibelle(String libelle) {
			this.libelle = libelle;
			return this;
		}

		public Builder setType(int type) {
			this.type = type;
			return this;
		}

		public Builder setLevel(int level) {
			this.level = level;
			return this;
		}

		public Builder setActeur(Acteur acteur) {
			this.acteur = acteur;
			return this;
		}

		public Builder setIgnoreUpdateCreator(boolean ignoreUpdateCreator) {
			this.ignoreUpdateCreator = ignoreUpdateCreator;
			return this;
		}

		public Evenement build() {
			Evenement evenement = new Evenement();
			evenement.setLevel(level);
			evenement.setLibelle(libelle);
			evenement.setType(type);
			evenement.setCreePar(acteur);
			evenement.setIgnoreUpdateCreator(ignoreUpdateCreator);
			return evenement;
		}

	}

	public boolean getNotifierActeurs() {
		return notifierActeurs;
	}

	public void setNotifierActeurs(boolean notifierActeurs) {
		this.notifierActeurs = notifierActeurs;
	}

	public boolean getActeursNotifies() {
		return acteursNotifies;
	}

	public void setActeursNotifies(boolean acteursNotifies) {
		this.acteursNotifies = acteursNotifies;
	}

}
