/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.model;

import com.ebenyx.template.jsf_spring.Constantes;
import javax.persistence.Entity;
import javax.persistence.Table;
/**
 *
 * @author desireadje
 */
@Entity
@Table(name = "database")
public class DatabaseService extends BaseEntity {
    public static final int TYPE_MYSQL = 1;
    public static final int TYPE_POSTGRES = 2;
    public static final int TYPE_SQLSERVER = 3;
    public static final int TYPE_ORACLE = 4;
    
    public static final int STATUS_ON = 1;
    public static final int STATUS_OFF = 2;
    
    private int type;
    
    private String ip;
    private String port;
    private String libelle;
    
    //access
    private String login;
    private String pass;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
    public String getLienJdbc(){
        return null;
    }
    
    
    
    
    
    
    
    
    
    public static DatabaseService db_pgsql() {
        DatabaseService db = new DatabaseService();
        db.setType(TYPE_POSTGRES);
        db.setIp("localhost");
        db.setPort("5432");
        db.setLibelle("poc_jar_db_pg");
        db.setLogin("postgres");
        db.setPass("postgres");

        return db;
    }
    
    public static DatabaseService db_mysql() {
        DatabaseService db = new DatabaseService();
        db.setType(TYPE_MYSQL);
        db.setIp("localhost");
        db.setPort("3306");
        db.setLibelle("poc_jar_db_mysql");
        db.setLogin("root");
        db.setPass("");

        return db;
    }
    
    public static DatabaseService db_orable() {
        DatabaseService db = new DatabaseService();
        db.setType(TYPE_ORACLE);
        db.setIp("localhost");
        db.setPort("8080");
        db.setLibelle("poc_jar_db_oracle");
        db.setLogin("sa");
        db.setPass("sa");

        return db;
    }
    
    public static DatabaseService db_sqlserver() {
        DatabaseService db = new DatabaseService();
        db.setType(TYPE_SQLSERVER);
        db.setIp("localhost");
        db.setPort("1433");
        db.setLibelle("poc_jar_db_sqlserver");
        db.setLogin("sa");
        db.setPass("sa");

        return db;
    }
}
