package com.ebenyx.template.jsf_spring.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.HashMap;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(
        indexes = {
            @Index(columnList = "type")
        }
)
public class Service extends BaseEntity {

    private String libelle;
    int delaisRafraichissement;
    private int type;

    @Lob
    private String proprieteJson;
    transient private HashMap<String, String> propriete;

    @Lob
    private String statusServiceJson;
    transient private StatutService statusService;

    public Service() {
        this.propriete = new HashMap<>();
    }

    
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDelaisRafraichissement() {
        return delaisRafraichissement;
    }

    public void setDelaisRafraichissement(int delaisRafraichissement) {
        this.delaisRafraichissement = delaisRafraichissement;
    }

    public String getStatusServiceJson() {
        return statusServiceJson;
    }

    public void setStatusServiceJson(String statusServiceJson) {
        this.statusServiceJson = statusServiceJson;
    }

    public void setStatusService(StatutService statusService) {
        this.statusService = statusService;
    }

<<<<<<< HEAD

    public StatutService getStatusService() {
        if (statusService == null) {
            try {
                statusService = new Gson().fromJson(statusServiceJson, StatutService.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return statusService;
=======
    public Statut getStatut() {
        /*if (statut == null) {
            statut = new Gson().fromJson(statusJson, Statut.class);
        }*/
        return statut;
>>>>>>> 8796fe04f7b109f6d878a75b09f1396855109cea
    }

    public String getProprieteJson() {
        return proprieteJson;
    }

    public void setProprieteJson(String proprieteJson) {
        this.proprieteJson = proprieteJson;
    }

    public HashMap<String, String> getPropriete() {
        if (propriete == null) {
            propriete = new Gson().fromJson(proprieteJson, new TypeToken<HashMap<String, String>>(){}.getType());
        }
        return propriete;
    }

    public void setPropriete(HashMap<String, String> propriete) {
        this.propriete = propriete;
    }

    public void dataToString() {
        this.statusServiceJson = new Gson().toJson(getStatusService());
    }

    
    public static class StatutService {
        private String libelle;
        private String couleur;
        private String[] details;
        private String erreur;

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        public String getCouleur() {
            return couleur;
        }

        public void setCouleur(String couleur) {
            this.couleur = couleur;
        }

        public String[] getDetails() {
            return details;
        }

        public void setDetails(String[] details) {
            this.details = details;
        }

        public String getErreur() {
            return erreur;
        }

        public void setErreur(String erreur) {
            this.erreur = erreur;
        }
    }

    public static class Statut {

        public Statut() {
        }
    }
}
