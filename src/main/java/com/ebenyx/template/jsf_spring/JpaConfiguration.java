package com.ebenyx.template.jsf_spring;

import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class JpaConfiguration implements ServletContextInitializer {

    // Fichier de base de données.
    public static final String H2DB = "jdbc:h2:file:~/sgci_distribution_carte_db";

    @Autowired
    private transient ApplicationProperties applicationProperties;

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setJpaProperties(additionalProperties());
        em.setPackagesToScan(new String[]{"com.ebenyx.template.jsf_spring.model"});

        /* new HibernateJpaVendorAdapter() */
        JpaVendorAdapter vendorAdapter = new EclipseLinkJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        // em.setLoadTimeWeaver(loadTimeWeaver());
        return em;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        // H2DB Config
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl(H2DB);
        dataSource.setUsername( "sa" );
        dataSource.setPassword( "" );
        
        
        /*dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/poc_jar_db");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres");*/

        return dataSource;  
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    public Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("eclipselink.logging.logger", "com.ebenyx.template.jsf_spring.Slf4jSessionLog");
        properties.setProperty("eclipselink.query-results-cache", "false");
        properties.setProperty("eclipselink.refresh", "true");
        properties.setProperty("toplink.cache.shared.default", "false");
        properties.setProperty("eclipselink.logging.level", "FINER");
        properties.setProperty("eclipselink.logging.level.sql", "FINER");
        properties.setProperty("eclipselink.logging.parameters", "true");
        properties.setProperty("javax.persistence.schema-generation.database.action", "create-or-extend-tables");
        properties.setProperty("eclipselink.weaving", "false");
        return properties;
    }

    @Override
    public void onStartup(ServletContext sc) throws ServletException {

    }

}
