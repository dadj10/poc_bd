/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.ebenyx.template.jsf_spring.facade.ActeurFacade;
import com.ebenyx.template.jsf_spring.facade.EvenementFacade;
import com.ebenyx.template.jsf_spring.model.Acteur;

/**
 * Classe de gestion de l'authentification
 *
 * @author ebenyx
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    ActeurFacade acteurFacade;

    @Autowired
    EvenementFacade evenementFacade;

    @Autowired
    protected HttpServletRequest request;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        Acteur acteur = null;

        if (authentication.getPrincipal() instanceof Acteur) {
            acteur = (Acteur) authentication.getPrincipal();
        } else {
            acteur = acteurFacade.authenticate(username, password);
        }

        if (acteur == null) {
            request.setAttribute("error", -1);
            throw new BadCredentialsException("Echec d'authentification !");
        }
        if (!acteur.isEnabled()) {
            request.setAttribute("error", -2);
            throw new BadCredentialsException("Le compte est desactivé");
        }
        evenementFacade.nouvelleConnexion(acteur);
        return new UsernamePasswordAuthenticationToken(acteur, username, acteur.getRoles());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
