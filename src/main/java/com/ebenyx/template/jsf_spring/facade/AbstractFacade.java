/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.facade;

import com.ebenyx.template.jsf_spring.model.Acteur;
import com.ebenyx.template.jsf_spring.model.BaseEntity;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 *
 * @author Ebony
 */
public abstract class AbstractFacade<T extends BaseEntity> {

    @Autowired
    private PlatformTransactionManager platformTransactionManager;

    public PlatformTransactionManager getPlatformTransactionManager() {
        return platformTransactionManager;
    }

    private Class<T> entityClass;
    public static final int MAX_RECORD_BY_QUERY = 100;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public Class<T> getEntityClass() {
        return entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public EntityManager getEM() {
        return getEntityManager();
    }

    public T getById(long id) {
        return getBy("id", id);
    }

    public T getBy(String field, Object value) {
        return (T) builder().where(field, value).findOneEntity();
    }

    public List<T> listBy(String field, Object value) {
        return builder().where(field, value).findAllEntity();
    }

    public void refresh(T entity) {
        getEntityManager().refresh(entity);
    }

    public void transaction(TransactionalProcedure run) {
        TransactionStatus vTransactionStatus = null;
        boolean active = TransactionSynchronizationManager.isActualTransactionActive();
        TransactionSynchronizationManager.isActualTransactionActive();
        if (!active) {
            vTransactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
        }
        try {
            run.run();
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.commit(vTransactionStatus);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.rollback(vTransactionStatus);
            }
            throw new RuntimeException(ex);
        }
    }

    public void create(T entity) {
        TransactionStatus vTransactionStatus = null;
        boolean active = TransactionSynchronizationManager.isActualTransactionActive();

        if (!active) {
            vTransactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
        }

        try {
            Acteur acteur = getCurrentActeur();
            if (acteur != null && entity.getCreePar() == null && (!entity.isIgnoreUpdateCreator())) {
                entity.setCreePar(acteur);
            }

            getEntityManager().persist(entity);
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.commit(vTransactionStatus);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.rollback(vTransactionStatus);
            }
            throw ex;
        }
    }

    public void edit(T entity) {
        edit(entity, true);
    }

    public void edit(T entity, boolean testContext) {
        TransactionStatus vTransactionStatus = null;
        boolean active = TransactionSynchronizationManager.isActualTransactionActive();
        if (!active) {
            vTransactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
        }
        try {
            Acteur acteur = getCurrentActeur();
            if (acteur != null && entity.getCreated() != null) {
                entity.setModifierPar(acteur);
            }
            getEntityManager().merge(entity);
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.commit(vTransactionStatus);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.rollback(vTransactionStatus);
            }
            throw ex;
        }

    }

    public void remove(T entity) {
        TransactionStatus vTransactionStatus = null;
        boolean active = TransactionSynchronizationManager.isActualTransactionActive();
        if (!active) {
            vTransactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
        }
        try {
            getEntityManager().remove(getEntityManager().merge(entity));
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.commit(vTransactionStatus);
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.rollback(vTransactionStatus);
            }
            throw ex;
        }

    }

    public T merge(T entity) {
        return getEntityManager().merge(entity);
    }

    private Path getPath(Root r, String field) {
        String[] tab = field.split("\\.");
        r.getModel().getAttribute(tab[0]);
        Path e = r.get(tab[0]);
        for (int i = 1; i < tab.length; i++) {
            e = e.get(tab[i]);
        }
        return e;
    }

    private Fetch fetch(Root r, String field, JoinType type) {
        String[] tab = field.split("\\.");
        r.getModel().getAttribute(tab[0]);
        Fetch e = r.fetch(tab[0], type);
        for (int i = 1; i < tab.length; i++) {
            e = e.fetch(tab[i], type);
        }
        return e;
    }

    protected Predicate condition(CriteriaBuilder cb, Root r, List<Object[]> where) {
        Predicate condition = cb.or(condition(true, cb, r, where.get(0)));
        for (int i = 1; i < where.size(); i++) {
            condition = cb.or(condition, condition(true, cb, r, where.get(i)));
        }
        return condition;
    }

    protected Predicate condition(CriteriaBuilder cb, Root r, Object[] param) {
        return condition(true, cb, r, param);
    }

    protected Predicate condition(boolean andCondition, CriteriaBuilder cb, Root r, Object... param) {
        Class objectClass;
        String[] tab;
        List<Predicate> predicateList = new ArrayList<>();
        for (int i = 0; i < param.length; i += 2) {
            try {
                if (param[i + 1] == null) {
                    predicateList.add(cb.isNull(getPath(r, param[i] + "")));
                    continue;
                }
                if (param[i].toString().endsWith(".or")) {
                    Object[] array = (Object[]) param[i + 1];
                    predicateList.add(condition(false, cb, r, array));
                    continue;
                }
                if (param[i].toString().endsWith(".and")) {
                    Object[] array = (Object[]) param[i + 1];
                    predicateList.add(condition(true, cb, r, array));
                    continue;
                }
                tab = ((String) param[i]).split(" ");
                objectClass = param[i + 1].getClass();
                if (!objectClass.isArray()) {
                    if (param[i + 1].toString().contains("%")) {
                        predicateList.add(cb.like(cb.upper(getPath(r, tab[0])), (param[i + 1] + "").toUpperCase()));

                    } else {
                        if (param[i + 1].toString().startsWith("$.")) {
                            predicateList.add(
                                    cb.equal(getPath(r, tab[0]), getPath(r, param[i + 1].toString().substring(2))));
                        } else {
                            predicateList.add(cb.equal(getPath(r, tab[0]), param[i + 1]));
                        }

                    }
                } else {
                    Object[] array = (Object[]) param[i + 1];
                    Path p = getPath(r, tab[0]);

                    if (array[1] instanceof Date) {
                        if (array[0].equals("between")) {
                            predicateList.add(cb.between(p.as(Date.class), (Date) array[1], (Date) array[2]));
                        } else if (array[0].equals("<")) {
                            predicateList.add(cb.lessThan(p.as(Date.class), (Date) array[1]));
                        } else if (array[0].equals("<=")) {
                            predicateList.add(cb.lessThanOrEqualTo(p.as(Date.class), (Date) array[1]));
                        } else if (array[0].equals(">=")) {
                            predicateList.add(cb.greaterThanOrEqualTo(p.as(Date.class), (Date) array[1]));
                        } else if (array[0].equals(">")) {
                            predicateList.add(cb.greaterThan(p.as(Date.class), (Date) array[1]));
                        } else {
                            predicateList.add(p.in(array));
                        }
                    } else if (array[1] instanceof Number) {
                        if (array[0].equals("between")) {
                            predicateList.add(cb.and(cb.ge(p.as(Number.class), (Number) array[1]),
                                    cb.le(p.as(Number.class), (Number) array[2])));
                        } else if (array[0].equals("<")) {
                            predicateList.add(cb.lt(p.as(Number.class), (Number) array[1]));
                        } else if (array[0].equals("<=")) {
                            predicateList.add(cb.le(p.as(Number.class), (Number) array[1]));
                        } else if (array[0].equals(">=")) {
                            predicateList.add(cb.gt(p.as(Number.class), (Number) array[1]));
                        } else if (array[0].equals(">")) {
                            predicateList.add(cb.ge(p.as(Number.class), (Number) array[1]));
                        } else if ("ne".equals(array[0])) {
                            predicateList.add(cb.notEqual(p, array[1]));
                        } else {
                            predicateList.add(p.in(array));
                        }
                    } else if ("in".equals(array[0])) {
                        predicateList.add(p.in((Collection) array[1]));
                    } else if ("not_in".equals(array[0])) {
                        predicateList.add(cb.not(p.in((Collection) array[1])));
                    } else if ("ne".equals(array[0])) {
                        predicateList.add(cb.notEqual(p, array[1]));
                    } else if ("contains".equals(array[0])) {
                        List<Predicate> list = new ArrayList<>();

                        for (Object obj : (Iterable<? extends Object>) array[1]) {
                            list.add(cb.isMember(obj, p));
                        }
                        predicateList.add(cb.or(list.toArray(new Predicate[0])));
                    }
                }
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
                continue;
            }
        }
        return andCondition ? cb.and(predicateList.toArray(new Predicate[0]))
                : cb.or(predicateList.toArray(new Predicate[0]));
    }

    public List<T> load(int first, int pageSize, String sortField, Boolean order, Map<String, Object> filters,
            String[] fetch) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        Root<T> r = cq.from(entityClass);
        cq.select(r);

        if (filters != null && (!filters.isEmpty())) {
            int i = 0;
            ArrayList<Object> list = new ArrayList<>();
            for (Map.Entry<String, Object> entrySet : filters.entrySet()) {
                String key = entrySet.getKey();
                Object value = entrySet.getValue();
                if (value != null && value.toString().length() != 0) {
                    list.add(key);
                    list.add(value);
                }
            }
            if (!list.isEmpty()) {
                cq.where(condition(cb, r, list.toArray(new Object[0])));
            }
        }
        if (order != null) {
            if (order) {
                cq.orderBy(cb.asc(getPath(r, sortField)));
            } else {
                cq.orderBy(cb.desc(getPath(r, sortField)));
            }
        } else {
            cq.orderBy(cb.desc(r.get("id")));
        }

        if (fetch != null && (fetch.length > 0)) {
            for (String field : fetch) {
                fetch(r, field, JoinType.LEFT);
            }
        }
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setFirstResult(first);
        q.setMaxResults(pageSize);
        return q.getResultList();
    }

    public QueryBuilder<T> builder() {
        return new QueryBuilder<T>(this);
    }

    void remove(QueryBuilder<T> queryBuilder) throws Exception {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaDelete<T> cd = cb.createCriteriaDelete(entityClass);
        Root<T> r = cd.from(entityClass);
        if (queryBuilder.where != null && (!queryBuilder.where.isEmpty())) {
            cd.where(condition(cb, r, queryBuilder.where.toArray()));
        }

        TransactionStatus vTransactionStatus = null;
        boolean active = TransactionSynchronizationManager.isActualTransactionActive();
        if (!active) {
            vTransactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
        }
        try {
            getEntityManager().createQuery(cd).executeUpdate();
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.commit(vTransactionStatus);
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.rollback(vTransactionStatus);
            }
            throw ex;
        }
    }

    void update(QueryBuilder<T> queryBuilder) throws Exception {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaUpdate<T> cd = cb.createCriteriaUpdate(entityClass);
        Root<T> r = cd.from(entityClass);
        if (queryBuilder.where != null && (!queryBuilder.where.isEmpty())) {
            cd.where(condition(cb, r, queryBuilder.where.toArray()));
        }

        if (queryBuilder.fieldValue != null) {
            for (Map.Entry<String, Object> field : queryBuilder.fieldValue.entrySet()) {
                cd.set(r.get(field.getKey()), field.getValue());
            }
        }
        TransactionStatus vTransactionStatus = null;
        boolean active = TransactionSynchronizationManager.isActualTransactionActive();
        if (!active) {
            vTransactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
        }
        try {
            getEntityManager().createQuery(cd).executeUpdate();
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.commit(vTransactionStatus);
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.rollback(vTransactionStatus);
            }
            throw ex;
        }
    }

    int executeUpdate(QueryBuilder<T> queryBuilder) {
        TransactionStatus vTransactionStatus = null;
        boolean active = TransactionSynchronizationManager.isActualTransactionActive();
        if (!active) {
            vTransactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
        }
        try {
            Query q = getEntityManager().createQuery(queryBuilder.query);
            if (queryBuilder.fieldValue != null) {
                for (Map.Entry<String, Object> field : queryBuilder.fieldValue.entrySet()) {
                    q.setParameter(field.getKey(), field.getValue());
                }
            }
            int result = q.executeUpdate();
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.commit(vTransactionStatus);
            }
            return result;
        } catch (Throwable ex) {
            ex.printStackTrace();
            if ((!active) && (!vTransactionStatus.isCompleted())) {
                platformTransactionManager.rollback(vTransactionStatus);
            }
            throw ex;
        }
    }

    List<Object[]> executeQuery(QueryBuilder<T> queryBuilder) {
        Query q = getEntityManager().createNativeQuery(queryBuilder.query);
        if (queryBuilder.fieldValue != null) {
            for (Map.Entry<String, Object> field : queryBuilder.fieldValue.entrySet()) {
                q.setParameter(field.getKey(), field.getValue());
            }
        }
        return q.getResultList();
    }

    List findAll(QueryBuilder<T> queryBuilder) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        Root<T> r = cq.from(entityClass);
        if (queryBuilder.selectAllEntityField()) {
            cq.select(r);
        } else {
            CompoundSelection<Tuple> cs = cb.tuple();
            if (queryBuilder.select != null && (!queryBuilder.select.isEmpty())) {
                for (String field : queryBuilder.select) {
                    cs.getCompoundSelectionItems().add(getPath(r, field).alias(field.replaceAll("\\.", "_")));
                }
            }

            if (queryBuilder.sum != null && (!queryBuilder.sum.isEmpty())) {
                for (String field : queryBuilder.sum) {
                    cs.getCompoundSelectionItems()
                            .add(cb.sum(r.<BigDecimal>get(field)).alias("sum_" + field.replaceAll("\\.", "_")));
                }
            }

            if (queryBuilder.count != null && (!queryBuilder.count.isEmpty())) {
                for (String field : queryBuilder.count) {
                    cs.getCompoundSelectionItems()
                            .add(cb.count(getPath(r, field)).alias("count_" + field.replaceAll("\\.", "_")));
                }
            }

            if (queryBuilder.avg != null && (!queryBuilder.avg.isEmpty())) {
                for (String field : queryBuilder.avg) {
                    cs.getCompoundSelectionItems()
                            .add(cb.avg(r.<BigDecimal>get(field)).alias("avg_" + field.replaceAll("\\.", "_")));
                }
            }
            cq.select(cs);
        }
        if (queryBuilder.fetch != null && (!queryBuilder.fetch.isEmpty())) {
            for (String field : queryBuilder.fetch) {
                // r.fetch(field,JoinType.LEFT);
                fetch(r, field, JoinType.LEFT);
            }
        }
        // queryBuilder.where.put("societe", societe);
        if (queryBuilder.where != null && (!queryBuilder.where.isEmpty())) {
            cq.where(condition(cb, r, queryBuilder.where.toArray()));
        }

        if (queryBuilder.having != null && (!queryBuilder.having.isEmpty())) {
            cq.having(condition(cb, r, queryBuilder.having.toArray()));
        }

        if (queryBuilder.groupBy != null && (!queryBuilder.groupBy.isEmpty())) {
            List<Expression> expressionList = new ArrayList<>();
            for (String field : queryBuilder.groupBy) {
                expressionList.add(getPath(r, field));
            }
            cq.groupBy(expressionList);
        }
        if (queryBuilder.orderBy != null && (!queryBuilder.orderBy.isEmpty())) {
            List<Expression> expressionList = new ArrayList<>();
            for (String field : queryBuilder.orderBy) {
                expressionList.add(getPath(r, field));
            }
            List<Order> orderList = new ArrayList<>();
            int index = 0;
            for (String orderField : queryBuilder.orderBy) {
                if (queryBuilder.orderList.get(index) == QueryBuilder.Order.ASC) {
                    orderList.add(cb.asc(getPath(r, orderField)));
                } else {
                    orderList.add(cb.desc(getPath(r, orderField)));
                }
                index++;
            }
            cq.orderBy(orderList);
        }
        if (queryBuilder.distinct) {
            cq.distinct(true);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        if (queryBuilder.limit != null) {
            q.setMaxResults(queryBuilder.limit);
        } else if (queryBuilder.from != null && queryBuilder.to != null) {
            q.setMaxResults(queryBuilder.to - queryBuilder.from + 1);
            q.setFirstResult(queryBuilder.from);
        }
        return q.getResultList();
    }

    public static interface TransactionalProcedure {

        void run() throws Exception;
    }

    public static Acteur getCurrentActeur() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication.getPrincipal() instanceof Acteur) {
                return (Acteur) authentication.getPrincipal();
            }
        } catch (Exception ex) {
        }
        return null;
    }
}
