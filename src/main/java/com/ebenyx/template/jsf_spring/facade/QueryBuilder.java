/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.facade;

import com.ebenyx.template.jsf_spring.model.BaseEntity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Tuple;

/**
 *
 * @author ebenyx
 */
public class QueryBuilder<T extends BaseEntity> {

	public static enum Order {
		ASC, DESC
	}

	List<String> select;
	List<Object> where;
	List<Object> having;
	List<String> orderBy;
	List<Order> orderList;
	List<String> groupBy;
	List<String> sum;
	List<String> count;
	List<String> avg;
	List<String> fetch;
	boolean distinct;

	String query;
	HashMap<String, Object> fieldValue;

	Integer limit;
	Integer from, to;

	AbstractFacade<T> facade;

	boolean remove;

	public QueryBuilder(AbstractFacade facade) {
		this.facade = facade;
		where = new ArrayList<>();
		having = new ArrayList<>();
	}

	public QueryBuilder select(String... field) {
		if (select == null) {
			select = new ArrayList<>();
		}
		for (String f : field) {
			select.add(f);
		}
		return this;
	}

	public QueryBuilder fetch(String... field) {
		if (fetch == null) {
			fetch = new ArrayList<>();
		}
		for (String f : field) {
			fetch.add(f);
		}
		return this;
	}

	public QueryBuilder whereMap(Map<String, Object> map) {
		for (Map.Entry es : map.entrySet()) {
			if (es.getValue() == null) {
				continue;
			}
			where.add(es.getKey());
			where.add(es.getValue());
		}
		return this;
	}

	public QueryBuilder where(Object... condition) {
		for (Object p : condition) {
			where.add(p);
		}
		return this;
	}

	public QueryBuilder having(Object... condition) {
		for (Object p : condition) {
			having.add(p);
		}
		return this;
	}

	public QueryBuilder orderBy(String field, Order order) {
		if (orderBy == null) {
			orderBy = new ArrayList<>();
			orderList = new ArrayList<>();
		}
		orderBy.add(field);
		orderList.add(order);
		return this;
	}

	public QueryBuilder groupBy(String... field) {
		if (groupBy == null) {
			groupBy = new ArrayList<>();
		}

		for (String f : field) {
			groupBy.add(f);
		}
		return this;
	}

	public QueryBuilder sum(String... field) {
		if (sum == null) {
			sum = new ArrayList<>();
		}
		for (String f : field) {
			sum.add(f);
		}
		return this;
	}

	public QueryBuilder count(String... field) {
		if (count == null) {
			count = new ArrayList<>();
		}
		for (String f : field) {
			count.add(f);
		}
		return this;
	}

	public QueryBuilder avg(String... field) {
		if (avg == null) {
			avg = new ArrayList<>();
		}
		for (String f : field) {
			avg.add(f);
		}
		return this;
	}

	public QueryBuilder limit(int maxResult) {
		this.limit = maxResult;
		return this;
	}

	public QueryBuilder range(int from, int to) {
		this.from = from;
		this.to = to;
		return this;
	}

	boolean selectAllEntityField() {
		return (select == null || select.isEmpty()) && (sum == null || sum.isEmpty())
				&& (count == null || count.isEmpty()) && (avg == null || avg.isEmpty());
	}

	public List<T> findAllEntity() {
		return build();
	}

	public List<Tuple> findAllTuple() {
		return build();
	}

	public T findOneEntity() {
		List<T> listT = findAllEntity();
		if (listT.isEmpty()) {
			return null;
		}
		limit(1);
		return listT.get(0);
	}

	public Tuple findOneTuple() {
		List<Tuple> listT = findAllTuple();
		if (listT.isEmpty()) {
			return null;
		}
		limit(1);
		return listT.get(0);
	}

	private List build() {
		return facade.findAll(this);
	}

	public void remove() throws Exception {
		facade.remove(this);
	}

	public QueryBuilder set(String field, Object value) {
		if (fieldValue == null) {
			fieldValue = new HashMap<>();
		}
		fieldValue.put(field, value);
		return this;
	}

	public void update() throws Exception {
		facade.update(this);
	}

	public QueryBuilder query(String query) {
		this.query = query;
		return this;
	}

	public QueryBuilder queryParam(String param, Object value) {
		return set(param, value);
	}

	public int executeUpdate() {
		return facade.executeUpdate(this);
	}

	public List<Object[]> executeQuery() {
		return facade.executeQuery(this);
	}

	public QueryBuilder<T> distinct() {
		distinct = true;
		return this;
	}
}
