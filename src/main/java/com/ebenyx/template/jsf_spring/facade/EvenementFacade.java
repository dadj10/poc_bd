package com.ebenyx.template.jsf_spring.facade;

import com.ebenyx.template.jsf_spring.model.Acteur;
import com.ebenyx.template.jsf_spring.model.Evenement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

@Component
public class EvenementFacade extends AbstractFacade<Evenement>{
    
    @PersistenceContext 
    EntityManager em;

    public EvenementFacade() {
        super(Evenement.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public void nouvelleConnexion(Acteur acteur){
        create(
            new Evenement.Builder(
                Evenement.CONNEXION,
                Evenement.VERBOSE,
                String.format("Nouvelle connexion :%s", acteur.getLibelle())
            )
            .setIgnoreUpdateCreator(true)
            .setActeur(acteur)
            .build()
        );
    }
    
    public void deconnexion(){
        Acteur acteur = getCurrentActeur();
        create(
            new Evenement.Builder(
                Evenement.DECONNEXION,
                Evenement.VERBOSE,
                String.format("Deconnexion :%s", acteur.getLibelle())
            )
            .setIgnoreUpdateCreator(true)
            .setActeur(acteur)
            .build()
        );
    }
    public void navigation(String desination){
        Acteur acteur = getCurrentActeur();
        create(
            new Evenement.Builder(
                Evenement.NAVIGATION,
                Evenement.VERBOSE,
                String.format("%s a consulter '%s'", 
                    acteur.getLibelle(),
                    desination
                )
            )
            .setIgnoreUpdateCreator(true)
            .setActeur(acteur)
            .build()
        );
    }

}


