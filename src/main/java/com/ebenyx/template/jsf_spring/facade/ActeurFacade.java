/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.facade;

import com.ebenyx.template.jsf_spring.model.Acteur;
import com.ebenyx.template.jsf_spring.utils.TimeUtil;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author ebenyx
 */
@Component
public class ActeurFacade extends AbstractFacade<Acteur> {

    private static final String SECRET = "wOcjcag5qaftWCzHMOybVCpcbLglvhW5L4x7zXeCSI0ssVQplBsS7cXb6IwretkK";

    @PersistenceContext
    EntityManager em;

    public ActeurFacade() {
        super(Acteur.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    // Cette fonnction permet de generer un nouveau mot de passe
    public static String getSecurePassword(String passwordToHash) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(SECRET.getBytes(StandardCharsets.UTF_8));
            byte[] bytes = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    // Cette fonction permet d'authentifier l'utilisateur
    public Acteur authenticate(String _login, String _password) {
        Acteur user = (Acteur) builder().where("login", _login).findOneEntity();
        if (user == null) {
            return null;
        } else {
            String pwd = getSecurePassword(_password);
            if (pwd.equals(user.getMotDePasse())) {
                return user;
            }
        }
        return null;
    }

    // Cette fonction permet de verifier la validité du token.
    public Acteur checkTokenValidite(String _token) {
        try {
            Acteur acteur = (Acteur) builder().where("token", _token).findOneEntity();
            if (acteur == null) {
                System.out.println("======> Token invalide.");
                return null;
            } else {
                Date tokenExpireDate = acteur.getTokenExpireDate();     // Date d'expiration du token. => Mon Oct 19 19:46:36 UTC 2020.
                Date dateActuelle = new Date();                         // Date actuelle.
                Long dateDiffInSeconds = TimeUtil.differenceEnSeconde(tokenExpireDate, dateActuelle);
                System.out.println("======> dateDiffInSeconds = " + dateDiffInSeconds);

                if (dateDiffInSeconds > 0) {
                    return acteur;
                } else {
                    System.out.println("======> Token a expiré.");
                }
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }

        return null;
    }

    // Cette fonction permet de retrouver un acteur a l'aide de son adresse email.
    public Acteur checkLogin(String _login) {
        Acteur user = (Acteur) builder().where("login", _login).findOneEntity();
        if (user != null) {
            return user;
        }
        return null;
    }
}
