/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.facade;

import com.ebenyx.template.jsf_spring.model.Parametre;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author ebenyx
 */
@Component
public class ParametreFacade extends AbstractFacade<Parametre>{
    
    @PersistenceContext 
    EntityManager em;

    public ParametreFacade() {
        super(Parametre.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public Parametre get() {
        Parametre parametre = builder().findOneEntity();
        if(parametre == null){
            parametre = new Parametre();
            create(parametre);
        } if(parametre.getTheme()==null){
            parametre.defaultTheme();
            edit(parametre);
        } return parametre;
    }
}


