/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.facade;

import com.ebenyx.template.jsf_spring.model.Role;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author ebenyx
 */
@Component
public class RoleFacade extends AbstractFacade<Role>{
    
    @PersistenceContext 
    EntityManager em;

    public RoleFacade() {
        super(Role.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}


