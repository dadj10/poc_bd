package com.ebenyx.template.jsf_spring.facade;

import com.ebenyx.template.jsf_spring.model.Moniteur;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

@Component
public class MoniteurFacade extends AbstractFacade<Moniteur>{
    
    @PersistenceContext 
    EntityManager em;

    public MoniteurFacade() {
        super(Moniteur.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}


