package com.ebenyx.template.jsf_spring.facade;

import com.ebenyx.template.jsf_spring.model.DatabaseService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

@Component
public class DatabaseServiceFacade extends AbstractFacade<DatabaseService>{
    
    @PersistenceContext 
    EntityManager em;

    public DatabaseServiceFacade() {
        super(DatabaseService.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}


