/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.ws;

import com.ebenyx.template.jsf_spring.facade.MoniteurFacade;
import com.ebenyx.template.jsf_spring.facade.ParametreFacade;
import com.ebenyx.template.jsf_spring.facade.ServiceFacade;
import com.ebenyx.template.jsf_spring.model.Moniteur;
import com.ebenyx.template.jsf_spring.model.Parametre;
import com.ebenyx.template.jsf_spring.model.Service;
import com.ebenyx.template.jsf_spring.model.Service.Statut;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/1")
public class WS {
    
    @Autowired
    private MoniteurFacade moniteurFacade;
    
    @Autowired
    private ParametreFacade parametreFacade;
    
    @Autowired
    private ServiceFacade serviceFacade;
    
    @RequestMapping(value = "/register",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public RegisterJsonResponse register(
            @RequestHeader("token") String token,
<<<<<<< HEAD
            @RequestBody MoniteurJson moniteurJson){
        
=======
            MoniteurJson moniteurJson){

>>>>>>> 8796fe04f7b109f6d878a75b09f1396855109cea
        Parametre parametre =  parametreFacade.get();
        if(!parametre.getWsToken().equals(token)){
            return new RegisterJsonResponse(-1);
        }
        try{
            Moniteur moniteur = (Moniteur) moniteurFacade
                    .builder()
                    .where("type",moniteurJson.type)
                    .findOneEntity();
            
            if(moniteur == null){
                moniteur = new Moniteur();
            }
            moniteur.init(moniteurJson);
            
            System.out.println("-- moniteur = " + new Gson().toJson(moniteur));
            
            moniteurFacade.edit(moniteur);
            List<Service> serviceList = serviceFacade
                    .builder().where("type", moniteurJson.type)
                    .findAllEntity();
            
            return new RegisterJsonResponse(
                    moniteur.getToken(),
                    parametre.getDelaisDeRafraichissementMoniteur(),
                    serviceList.stream()
                            .map(s->new ServiceJson(s))
                            .collect(Collectors.toList())
                );
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
        return new RegisterJsonResponse(-2);
    }
    
    
    @RequestMapping(value = "/update_status_service",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public int update(
            @RequestHeader("token") String token,
            UpdateStatusServiceRequest updateResponse){
        
        Moniteur moniteur =(Moniteur)moniteurFacade.builder().where("token",token).findOneEntity();
        if(moniteur == null){
            return -1;
        }
        Service service;
        for(Map.Entry<Long, Service.StatutService> entry:updateResponse.data.entrySet()){
            service = (Service) serviceFacade.builder().where("type",moniteur.getType(),"id",entry.getKey()).findOneEntity();
            if(service == null){
                continue;
            }
            service.setStatusService(entry.getValue());
            service.dataToString();
            serviceFacade.edit(service);
        }
        return 0;
    }
    
    public static class UpdateStatusServiceRequest{
        private HashMap<Long,Service.StatutService> data;
        public HashMap<Long, Service.StatutService> getData() {
            return data;
        }
        public void setData(HashMap<Long, Service.StatutService> data) {
            this.data = data;
        }
    }
    
    public static class RegisterJsonResponse{
        int code;
        int delaisDeRafraichissementMoniteur;
        String token;
        
        List<ServiceJson> serviceJsonList;
        public RegisterJsonResponse(int code){
            this.code = code;
        }
        
        public RegisterJsonResponse(String token,int delaisDeRafraichissementMoniteur, List<ServiceJson> serviceJsonList) {
            this.token = token;
            this.delaisDeRafraichissementMoniteur = delaisDeRafraichissementMoniteur;
            this.serviceJsonList = serviceJsonList;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public int getDelaisDeRafraichissementMoniteur() {
            return delaisDeRafraichissementMoniteur;
        }

        public void setDelaisDeRafraichissementMoniteur(int delaisDeRafraichissementMoniteur) {
            this.delaisDeRafraichissementMoniteur = delaisDeRafraichissementMoniteur;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public List<ServiceJson> getServiceJsonList() {
            return serviceJsonList;
        }

        public void setServiceJsonList(List<ServiceJson> serviceJsonList) {
            this.serviceJsonList = serviceJsonList;
        }
    }
    
    public static class ServiceJson{
        long id;
        int delaisRafraichissement;
        HashMap<String, String> propriete;
        Service.StatutService statut;

        public ServiceJson(Service service) {
            this.id = service.getId();
            this.delaisRafraichissement = service.getDelaisRafraichissement();
            this.propriete = service.getPropriete();
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public int getDelaisRafraichissement() {
            return delaisRafraichissement;
        }

        public void setDelaisRafraichissement(int delaisRafraichissement) {
            this.delaisRafraichissement = delaisRafraichissement;
        }

        public HashMap<String, String> getPropriete() {
            return propriete;
        }

        public void setPropriete(HashMap<String, String> propriete) {
            this.propriete = propriete;
        }

        public Service.StatutService getStatut() {
            return statut;
        }

        public void setStatut(Service.StatutService statut) {
            this.statut = statut;
        }
    }
    
    public static class MoniteurJson{
        private int type;
        private String libelle;
        private List<Moniteur.Champ> champList;

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        public List<Moniteur.Champ> getChampList() {
            return champList;
        }

        public void setChampList(List<Moniteur.Champ> champList) {
            this.champList = champList;
        }
    }
}
