/**
 *  (C) 2013-2014 Stephan Rauh http://www.beyondjava.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ebenyx.template.jsf_spring.controller;
import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.ParametreFacade;
import com.ebenyx.template.jsf_spring.model.BaseEntity;
import com.ebenyx.template.jsf_spring.model.Parametre;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import org.springframework.beans.factory.annotation.Autowired;

@ManagedBean
@ApplicationScoped
public class ApplicationController extends AbstractController<BaseEntity>{

    @Autowired
    private ParametreFacade parametreFacade;
    
    private Parametre parametre;
    
    @Override
    public AbstractFacade getFacade() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public BaseEntity newEntity() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Parametre getParametre() {
        if(parametre == null){
            parametre = parametreFacade.get();
        }
        return parametre;
    }
    
    public void saveParam(){
        parametreFacade.edit(parametre);
    }
    
}