/**
 *  (C) 2013-2014 Stephan Rauh http://www.beyondjava.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ebenyx.template.jsf_spring.controller;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.ebenyx.template.jsf_spring.JsfUtil;
import com.ebenyx.template.jsf_spring.Main;
import com.ebenyx.template.jsf_spring.MyContext;
import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.ParametreFacade;
import com.ebenyx.template.jsf_spring.job.mail.Mail;
import com.ebenyx.template.jsf_spring.model.BaseEntity;
import com.ebenyx.template.jsf_spring.model.Parametre;
import com.ebenyx.template.jsf_spring.model.Acteur;
import com.ebenyx.template.jsf_spring.service.EmailService;
import com.google.gson.Gson;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.primefaces.model.UploadedFileWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@ManagedBean
@SessionScoped
public class ConfigController extends AbstractController<BaseEntity> {

    final static private Logger logger = LoggerFactory.getLogger(ConfigController.class);

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private EmailService emailService;

    private Parametre parametre;

    @Autowired
    private ParametreFacade parametreFacade;

    @Autowired
    private MyContext myContext;

    @Autowired
    private HttpServletRequest request;

    private File currentFolder;

    @Override
    public AbstractFacade getFacade() {
        return parametreFacade;
    }

    public boolean isConnected() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (!(authentication instanceof AnonymousAuthenticationToken));
    }

    @Override
    public BaseEntity newEntity() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Parametre getParametre() {
        if (parametre == null) {
            parametre = parametreFacade.get();
        }
        return parametre;
    }

    public void reloadConfigJson() {
        set("configJson", new Gson().toJson(parametreFacade.get()));
    }

    public void saveConfigJson() {
        try {
            Gson gson = new Gson();
            Parametre parametre = gson.fromJson(getString("configJson"), Parametre.class);
            //remove config
            parametreFacade.builder().remove();
            parametreFacade.create(parametre);
            this.parametre = parametre;
            JsfUtil.msgSucces();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveParam() {
        parametreFacade.edit(parametre);
        JsfUtil.msgSucces();
    }

    public Acteur getUserAct() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof Acteur) {
            return (Acteur) authentication.getPrincipal();
        }
        return null;
    }

    public boolean getAnonymousUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof String) {
            return "anonymousUser".equals(authentication.getPrincipal());
        }
        return false;
    }

    public void saveEmailParam() {
        try {
            parametre.setup((JavaMailSenderImpl) emailSender);
            emailService._sendSimpleMessage(
                    Mail.createNew()
                            .setTo(parametre.getMailTest())
                            .setSubject("EMAIL TEST")//config("app_name"))
                            .setTemplate("email-test.html")
            );
            saveParam();
        } catch (Exception ex) {
            parametreFacade.edit(parametre);
            ex.printStackTrace();
            JsfUtil.msgErreur("Echec de connexion !");
        }
    }

    public List<File> currentFolderContent() {
        if (get("currentFolderContent") == null) {
            set("currentFolderContent", Arrays.asList(getCurrentFolder().listFiles()));
        }
        return (List) get("currentFolderContent");
    }

    public void setCurrentFolder(File currentFolder) {
        unsetField("currentFolderContent");
        this.currentFolder = currentFolder;
    }

    public File getCurrentFolder() {
        if (currentFolder == null) {
            currentFolder = new File(myContext.getBasePath());
        }
        return currentFolder;
    }

    public void remplacerFileAndRestart() {
        if (_remplacerFile()) {
            redemarrer();
        }
    }

    public void remplacerFile() {
        _remplacerFile();
    }

    public boolean _remplacerFile() {
        try {
            UploadedFileWrapper fileWrapper = (UploadedFileWrapper) get("remplacerFile");
            if (fileWrapper == null) {
                JsfUtil.msgErreur("Impossible de joindre le fichier");
                return false;
            }
            FileUtils.copyInputStreamToFile(
                    fileWrapper.getInputstream(),
                    new File(String.format("%s/%s", getCurrentFolder().getAbsolutePath(), fileWrapper.getFileName()))
            );
            JsfUtil.msgSucces();
            unsetField("currentFolderContent");
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            JsfUtil.msgErreur(ex.getMessage());
        }
        return false;
    }

    public void redemarrer() {
        Main.restart();
        JsfUtil.msgSucces("Redemerrage en cours ...");
        logger.info("redemerrage en cours ...");
    }

    public String handleCommand(String command, String[] params) {
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec(command + " " + String.join(" ", params), null, currentFolder);
            Scanner s = new Scanner(process.getInputStream());
            StringBuilder text = new StringBuilder();
            while (s.hasNextLine()) {
                text.append(s.nextLine());
                text.append("\r\n");
            }
            s.close();
            int result = process.waitFor();
            unsetField("currentFolderContent");
            return text.toString();
        } catch (Exception ex) {
            logger.error("", ex);
            return ex.getMessage();
        }
    }
}
