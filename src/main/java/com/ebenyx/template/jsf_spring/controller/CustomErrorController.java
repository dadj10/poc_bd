/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import static org.springframework.core.NestedExceptionUtils.getRootCause;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CustomErrorController implements ErrorController {

    @RequestMapping("/error")
    public ModelAndView handleError(HttpServletRequest request) {
        ModelAndView errorPage = null;
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
        String exceptionMessage = "";
        
        try {
            exceptionMessage = getExceptionMessage(throwable, (Integer) status);
        } catch (Exception ex) {
        }
        
        errorPage = new ModelAndView("error");
        errorPage.addObject("code", status);
        errorPage.addObject("message", exceptionMessage);
        
        return errorPage;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    private String getExceptionMessage(Throwable throwable, Integer statusCode) {
        if (throwable != null) {
            return getRootCause(throwable).getMessage();
        }
        
        HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
        
        return httpStatus.getReasonPhrase();
    }
}
