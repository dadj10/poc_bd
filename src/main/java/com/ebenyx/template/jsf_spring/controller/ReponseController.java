package com.ebenyx.template.jsf_spring.controller;

import com.ebenyx.template.jsf_spring.JsfUtil;
import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.ActeurFacade;
import com.ebenyx.template.jsf_spring.model.Acteur;
import com.ebenyx.template.jsf_spring.model.BaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

@ManagedBean
@ViewScoped
public class ReponseController extends AbstractController<BaseEntity> {

    @Autowired
    private transient ActeurFacade acteurFacade;

    @Override
    public AbstractFacade getFacade() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BaseEntity newEntity() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void prepareConnexion(String token, String login) {
        if (!isPageReloaded()) {
            return;
        }

        Acteur acteur
                = (Acteur) acteurFacade.builder()
                        .where("uuid", token, "login", login).findOneEntity();
        if (acteur == null) {
            JsfUtil.msgErreur("Le lien est erronée ou a expiré !");
            return;
        }
        //on connecte l'acteur
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                acteur,
                acteur.getLogin(),
                acteur.getRoles()
        ));
        acteur.updateUuid();
        acteurFacade.edit(acteur);
        redirectAfterDelay("/admin/index.xhtml", 3000);
        JsfUtil.msgSucces("Connexion effectué, vous serez redirigez dans 3 secondes.");
    }
}
