package com.ebenyx.template.jsf_spring.controller;
import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.EvenementFacade;
import com.ebenyx.template.jsf_spring.model.Acteur;
import com.ebenyx.template.jsf_spring.model.Evenement;
import javax.faces.bean.ManagedBean;
import org.springframework.beans.factory.annotation.Autowired;
import javax.faces.bean.SessionScoped;
import java.util.List;
import java.util.stream.Collectors;

@ManagedBean
@SessionScoped
public class NotificationController extends AbstractController<Evenement>{
    
    private List<Acteur> acteurSelectedList;
    
    @Autowired
    private transient EvenementFacade facade;
    
    @Override
    public AbstractFacade getFacade() {
        return facade;
    }
    
    @Override
    public Evenement newEntity() {
        return new Evenement();
    }

    public List<Acteur> getActeurSelectedList() {
        return acteurSelectedList;
    }

    public void setActeurSelectedList(List<Acteur> acteurSelectedList) {
        this.acteurSelectedList = acteurSelectedList;
    }
    
    @Override
    protected void beforeLoadLasyData() {
        if(acteurSelectedList == null || acteurSelectedList.isEmpty()){
            unsetField("acteur");
        }else{
            set("acteur",new Object[]{"in",acteurSelectedList.stream().map(p->p.getId()).collect(Collectors.toList())});
        }
        lazyItemList.reset();
    }
}
