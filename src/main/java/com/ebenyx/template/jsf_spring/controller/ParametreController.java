package com.ebenyx.template.jsf_spring.controller;
import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.ParametreFacade;
import com.ebenyx.template.jsf_spring.model.Parametre;
import javax.faces.bean.ManagedBean;
import org.springframework.beans.factory.annotation.Autowired;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ParametreController extends AbstractController<Parametre>{
    
    @Autowired
    private transient ParametreFacade facade;
    
    @Override
    public AbstractFacade getFacade() {
        return facade;
    }
    
    @Override
    public Parametre newEntity() {
        return null;
    }

    @Override
    public Parametre getSelected() {
        if(selected == null){
            selected = facade.get();
        }
        return super.getSelected(); //To change body of generated methods, choose Tools | Templates.
    }
}
