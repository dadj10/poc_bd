package com.ebenyx.template.jsf_spring.controller;
import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.DatabaseServiceFacade;
import com.ebenyx.template.jsf_spring.model.DatabaseService;
import javax.faces.bean.ManagedBean;
import org.springframework.beans.factory.annotation.Autowired;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class DatabaseServiceController extends AbstractController<DatabaseService>{
    
    @Autowired
    private transient DatabaseServiceFacade facade;
    
    @Override
    public AbstractFacade getFacade() {
        return facade;
    }
    
    @Override
    public DatabaseService newEntity() {
        return new DatabaseService();
    }
}
