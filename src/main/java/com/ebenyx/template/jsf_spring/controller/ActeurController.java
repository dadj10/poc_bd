/**
 *  (C) 2013-2014 Stephan Rauh http://www.beyondjava.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ebenyx.template.jsf_spring.controller;

import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.model.Acteur;
import com.ebenyx.template.jsf_spring.facade.ActeurFacade;
import com.ebenyx.template.jsf_spring.service.EmailService;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;

@ManagedBean
@SessionScoped
public class ActeurController extends AbstractController<Acteur> {

    @Autowired
    private transient ActeurFacade facade;

    @Autowired
    private transient EmailService emailService;

    @Autowired
    private HttpServletRequest request;

    @Override
    protected String[] lazyFetch() {
        return new String[]{};
    }

    @Override
    public AbstractFacade getFacade() {
        return facade;
    }

    @Override
    public Acteur newEntity() {
        return new Acteur();
    }

    @Override
	public void refresh() {
        itemList = facade.builder().findAllEntity();
    }

    @Override
    protected void beforeLoadLasyData() {
        String filtre = getString("filtre");
        if (filtre.isEmpty()) {
            unsetField("filtre.or");
        } else {
            filtre = "%" + filtre + "%";
            set("filtre.or", new Object[]{
                "nom", filtre,
                "prenoms", filtre,
                "email", filtre
            });
        }
    }
}
