package com.ebenyx.template.jsf_spring.controller;

import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.ServiceFacade;
import com.ebenyx.template.jsf_spring.model.Service;
import javax.faces.bean.ManagedBean;
import org.springframework.beans.factory.annotation.Autowired;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ServiceController extends AbstractController<Service> {

    @Autowired
    private transient ServiceFacade facade;

    @Override
    public AbstractFacade getFacade() {
        return facade;
    }

    @Override
    public Service newEntity() {
        return new Service();
    }
    
            
            
}
