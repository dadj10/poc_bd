package com.ebenyx.template.jsf_spring.controller;
import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.MoniteurFacade;
import com.ebenyx.template.jsf_spring.facade.ServiceFacade;
import com.ebenyx.template.jsf_spring.model.Moniteur;
import com.ebenyx.template.jsf_spring.model.Moniteur.Champ;
import com.ebenyx.template.jsf_spring.model.Service;
import javax.faces.bean.ManagedBean;
import org.springframework.beans.factory.annotation.Autowired;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class MoniteurController extends AbstractController<Moniteur>{
    
    @Autowired
    private transient MoniteurFacade facade;
    @Autowired
    private transient ServiceFacade serviceFacade;
    
    @Override
    public AbstractFacade getFacade() {
        return facade;
    }
    
    @Override
    public Moniteur newEntity() {
        return new Moniteur();
    }
    
    public void creerService(){
        Service service = new  Service();
        service.setLibelle(getSelected().getLibelle());
        for(Champ champ : getSelected().getChampList()){
             service.getPropriete().put(champ.getLibelle(), getString(champ.getLibelle()));
        }
        //renseigner les autres parametres
        service.dataToString();
        serviceFacade.create(service);
    }
}
