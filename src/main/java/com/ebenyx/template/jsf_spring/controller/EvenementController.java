package com.ebenyx.template.jsf_spring.controller;
import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.EvenementFacade;
import com.ebenyx.template.jsf_spring.model.Evenement;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class EvenementController extends AbstractController<Evenement>{
    
    @PersistenceContext
    private EntityManager em;
    
    @Autowired
    private transient EvenementFacade facade;
    
    @Override
    public AbstractFacade getFacade() {
        return facade;
    }
    
    @Override
    public Evenement newEntity() {
        return new Evenement();
    }
}
