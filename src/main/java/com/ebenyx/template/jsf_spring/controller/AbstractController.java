package com.ebenyx.template.jsf_spring.controller;

import com.ebenyx.template.jsf_spring.JsfUtil;
import com.ebenyx.template.jsf_spring.MyContext;
import com.ebenyx.template.jsf_spring.PersistAction;
import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.EvenementFacade;
import com.ebenyx.template.jsf_spring.facade.QueryBuilder;
import com.ebenyx.template.jsf_spring.model.Acteur;
import com.ebenyx.template.jsf_spring.model.BaseEntity;
import com.ebenyx.template.jsf_spring.service.EmailService;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.sun.faces.component.visit.FullVisitContext;
import freemarker.template.Template;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFileWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfig;

public abstract class AbstractController<T extends BaseEntity> implements Serializable {

    public static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String PATTERN_PHONE = "\\d{8}";
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
                Pattern.CASE_INSENSITIVE);

    @Autowired
    protected transient EmailService emailService;

    @Autowired
    protected HttpServletRequest request;

    public static int RESULT_PHOTO = 1;
    private List<T> itemSelectedList;
    protected List<T> itemList = null, importItemList;
    protected AbstractLazyModel lazyItemList;
    protected T selected;
    protected PersistAction action;
    private MyHashMap field;
    private PrintStream currentReportPs;
    protected File currentReportFile;
    private boolean showReport;
    private String currentReportName;

    @Autowired
    protected MyContext myContext;

    @Autowired
    FreeMarkerConfig freemarkerConfig;

    @Autowired
    protected transient EvenementFacade evenementFacade;

    public AbstractController() {
        field = new MyHashMap();
        buildLazyModel();
    }

    protected void buildLazyModel() {
        lazyItemList = new AbstractLazyModel();
    }

    @PostConstruct
    private void init() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        ServletContext servletContext = (ServletContext) externalContext.getContext();
        WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext).getAutowireCapableBeanFactory()
                .autowireBean(this);
        postConstruct();
    }

    protected void postConstruct() {
    }

    public String prepareUpdateWithIndex(int index) {
        setSelected(getItemList().get(index));
        return prepareUpdate();
    }

    public String prepareUpdateWithId(long id) {
        setSelected((T) getFacade().builder().where("id", id).findOneEntity());
        return prepareUpdate();
    }

    public void selectIndex(int index) {
        setSelected(getItemList().get(index));
        prepareUpdate();
    }

    public void selectId(long id) {
        setSelected((T) getFacade().builder().where("id", id).findOneEntity());
        prepareUpdate();
    }

    public void edit(T entity) {
        setSelected(entity);
        update();
    }

    public T getSelected() {
        if (selected != null) {
            return selected;
        } else if (!getItemSelectedList().isEmpty()) {
            return getItemSelectedList().get(0);
        }
        return null;
    }

    public void setSelected(T selected) {
        this.selected = selected;
        if (selected != null && selected.getId() != null) {
            this.action = PersistAction.UPDATE;
            prepareUpdate();
        }
    }

    public List<T> getImportItemList() {
        return importItemList;
    }

    public void setImportItemList(List<T> importItemList) {
        this.importItemList = importItemList;
    }

    public List<T> getItemList() {
        if (itemList == null) {
            refresh();
        }
        return itemList;
    }

    public List<T> getList() {
        return getFacade().builder().findAllEntity();
    }

    public void setItemList(List<T> itemList) {
        this.itemList = itemList;
    }

    public PersistAction getAction() {
        return action;
    }

    public void setAction(PersistAction action) {
        this.action = action;
    }

    public abstract AbstractFacade getFacade();

    public abstract T newEntity();

    public void reset() {
        selected = null;
    }

    public void select(T entity) {
        setSelected(entity);
    }

    public void createIfNull() {
        if (getSelected() == null) {
            prepareCreate();
        }
    }

    public String logout(String page) {
        evenementFacade.deconnexion();
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return redirectTo(page);
    }

    public String prepareCreate() {
        return prepareCreateEntity(null);
    }

    public String prepareCreateEntity(String args) {
        action = PersistAction.CREATE;
        selected = newEntity();
        return "";
    }

    public String prepareUpdate() {
        return prepareUpdateEntity(null);
    }

    public String prepareUpdateEntity(String args) {
        action = PersistAction.UPDATE;
        return null;
    }

    public void update() {
        prepareUpdate();
        save();
    }

    public void insert() {
        action = PersistAction.CREATE;
        save();
    }

    public void prepareDelete() {
        action = PersistAction.DELETE;
    }

    public void prepareDeleteWithIndex(int index) {
        setSelected(itemList.get(index));
        prepareDelete();
    }

    public void prepareDeleteWithId(long id) {
        setSelected((T) getFacade().builder().where("id", id).findOneEntity());
        prepareDelete();
    }

    public boolean isActionCreate() {
        return action == PersistAction.CREATE;
    }

    public boolean isActionUpdate() {
        return action == PersistAction.UPDATE;
    }

    public boolean isActionRemove() {
        return action == PersistAction.DELETE;
    }

    protected boolean isValide() {
        return true;
    }

    public void beforeCreate() throws Exception {

    }

    public void beforeSave() throws Exception {
    }

    public void afterSave() throws Exception {
    }

    public void afterCreate() throws Exception {
    }

    public void beforeUpdate() throws Exception {

    }

    public void afterUpdate() throws Exception {
    }

    public void beforeRemove() throws Exception {

    }

    public void afterRemove() throws Exception {
    }

    public void endSaveTransaction() {
    }

    public void save() {
        try {
            if (!isValide()) {
                FacesContext.getCurrentInstance().validationFailed();
                JsfUtil.msgErreurSaisieIncoherente();
                return;
            }
            if (selected == null) {
                JsfUtil.msgErreur("aucun_element_a_enregistre");
                return;
            }

            if (itemList == null) {
                itemList = new ArrayList<>();
            }

            getFacade().transaction(() -> {
                if (action == PersistAction.CREATE) {
                    selected.setCreePar(getCurrentActeur());
                    beforeSave();
                    beforeCreate();
                    getFacade().create(selected);
                    if (lazyItemList.getItemList() != null) {
                        lazyItemList.getItemList().add(selected);
                    }
                    itemList.add(selected);
                    lazyItemList.reset();
                    afterSave();
                    afterCreate();
                } else if (action == PersistAction.UPDATE) {
                    selected.setModifierPar(getCurrentActeur());
                    beforeSave();
                    beforeUpdate();
                    getFacade().edit(selected);
                    afterSave();
                    afterUpdate();
                } else if (action == PersistAction.DELETE) {
                    beforeRemove();
                    getFacade().remove(selected);
                    itemList.remove(selected);
                    afterRemove();
                    setSelected(null);
                }
            });
            if (action == PersistAction.CREATE) {
                JsfUtil.msgSuccesCreate();
                action = PersistAction.UPDATE;
            } else if (action == PersistAction.UPDATE) {
                JsfUtil.msgSuccesUpdate();
            } else if (action == PersistAction.DELETE) {
                JsfUtil.msgSuccesRemove();
            }
            endSaveTransaction();
        } catch (Exception ex) {
            JsfUtil.msgErreur("Une erreur est survenue lors du traitement des informations.");
        }

    }

    public void removeItemSelectedList() {
        for (T entity : getItemSelectedList()) {
            getFacade().remove(entity);
        }
    }

    public void delete(int index) {
        setSelected(itemList.get(index));
        remove();
    }

    public void remove() {
        action = PersistAction.DELETE;
        save();
    }

    public void removeObject(T entity) {
        T old_selected = selected;
        selected = entity;

        remove();

        selected = old_selected;
    }

    public T getEntity(java.lang.Integer id) {
        return (T) getFacade().builder().where("id", id).findOneEntity();
    }

    public AbstractLazyModel getLazyItemList() {
        return lazyItemList;
    }

    public void setLazyItemList(AbstractLazyModel lazyItemList) {
        this.lazyItemList = lazyItemList;
    }

    public List<T> getItemSelectedList() {
        if (itemSelectedList == null) {
            itemSelectedList = new ArrayList<>();
        }
        return itemSelectedList;
    }

    public void setItemSelectedList(List<T> itemSelectedList) {
        this.itemSelectedList = itemSelectedList;
    }

    public void toggleSelect(int index) {
        T elt = getItemList().get(index);
        elt.toogleSelect();
        if (elt.getChecked()) {
            addToItemSelectedList(index);
        } else {
            removeToItemSelectedList(index);
        }
    }

    public void toggleSelectLasy(int index) {
        T elt = lazyItemList.getItemByIndex(index);
        elt.toogleSelect();
        if (elt.getChecked()) {
            addToItemSelectedList(index);
        } else {
            removeToItemSelectedList(index);
        }
    }

    public void addToItemSelectedList(int index) {
        getItemSelectedList().add(getItemList().get(index));
        // getItemList().remove(index);
    }

    public void removeToItemSelectedList(int index) {
        getItemSelectedList().remove(getItemList().get(index));
        // getItemSelectedList().remove(index);
    }

    public void addObjectToItemSelectedList(T object) {
        getItemSelectedList().add(object);
    }

    public void removeObjectToItemSelectedList(T object) {
        getItemSelectedList().remove(object);
    }

    public String redirectTo(String page) {
        if (page == null) {
            return null;
        }
        return String.format("%s?faces-redirect=true", page);
    }

    public Boolean _equal(String key, Object value) {
        return value.equals(get(key));
    }

    public Boolean _boolean(String key) {
        if (!field.containsKey(key)) {
            field.put(key, Boolean.FALSE);
        }
        return Boolean.parseBoolean(field.get(key) + "");
    }

    public void _booleanToggle(String key) {
        field.put(key, !_boolean(key));
        if ("view_all_data".equals(key)) {
            for (T elt : getItemSelectedList()) {
                elt.setChecked(false);
            }
            getItemSelectedList().clear();
        } else if ("moreSearchOpt".equals(key)) {
            field.put("view_all_data", false);
        }
    }

    public HashMap<String, Object> getField() {
        return field;
    }

    public void setField(MyHashMap field) {
        this.field = field;
    }

    public void set(String field, Object object) {
        this.field.put(field, object);
    }

    public void setDefault(String field, Object object, Object defaultValue) {
        set(field, object == null ? defaultValue : object);
    }

    public Object get(String name) {
        return field.get(name);
    }

    public int _length(String name) {
        try {
            return ((Object[]) get(name)).length;
        } catch (Exception exs) {
            return 0;
        }

    }

    public String getString(String name, String defaultValue) {
        Object value = get(name);
        if (value == null) {
            set(name, defaultValue);
            return defaultValue;
        }
        return value.toString();
    }

    public String getString(String name) {
        return getString(name, "");
    }

    public String to(String page) {
        return redirectTo(page);
    }

    public String selectAndGoto(int index, String page) {
        selectIndex(index);
        return to(page);
    }

    public String selectObjectAndGoto(T object, String page) {
        setSelected(object);
        return to(page);
    }

    public void selectIdAndUpdate(long id) {
        selectId(id);
        save();
    }

    public void selectObjectAndUpdate(T entity) {
        setSelected(entity);
        save();
    }

    public void selectIdAndRemove(long id) {
        selectId(id);
        remove();
    }

    public String selectIdAndGoto(long id, String page) {
        selectId(id);
        return to(page);
    }

    public Object getDefaultIfNull(String key, Object val) {
        if (field.containsKey(key)) {
            return field.get(key);
        }
        field.put(key, val);
        return val;
    }

    protected List<T> listFromAutoComplete(String field, String query, int limit) {
        return getFacade().builder().limit(limit).where(field, query + "%").findAllEntity();
    }

    public String autocompleteSelectAndGoto(String page) {
        setSelected((T) get("autocomplete.selected"));
        set("autocomplete.selected", null);
        return redirectTo(page);
    }

    public List<T> last(int limit, Object... param) {
        return getFacade().builder().limit(limit).where(param).findAllEntity();
    }

    public List<T> lastBy(int limit, String field) {
        return getFacade().builder().limit(limit).orderBy(field, QueryBuilder.Order.DESC).findAllEntity();
    }

    public String gotoPage() {
        Map<String, String> map = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        try {
            String page = map.get("page") + "";
            System.out.println("page vaut " + page);
            redirectTo(page);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    public void prepareFilter(String fieldsString) {
        lazyItemList.setRowCount(-1);
        String[] fields = fieldsString.split(",");
        ArrayList<String> tags = (ArrayList<String>) get("_filterChips");
        set("_filter.chips.or", null);
        set("_filter.chips.and", null);
        if (tags == null || tags.isEmpty()) {
            return;
        }
        String comb = getString("_filter.chips.comb", "or");
        ArrayList<Object> condition = new ArrayList<>();
        ArrayList<Object> c;
        for (String tag : tags) {
            c = new ArrayList<>();
            for (String field : fields) {
                c.add(field);
                c.add(tag + "%");
            }
            condition.add("_filter.chips.or");
            condition.add(c.toArray());
        }
        set(String.format("_filter.chips.%s", comb), condition.toArray());
    }

    protected void prepareSearch(String... fields) {
        String value;
        for (String field : fields) {
            value = getString(String.format("_%s", field));
            if (value.length() > 0) {
                set(field, value + "%");
            } else {
                getField().remove(field);
            }
        }
        lazyItemList.setRowCount(-1);
    }

    public void prepareSearchByDateBetween(String field) {
        prepareSearchByDateBetween(field, true);
    }

    public void setAsToday(String field) {
        Date[] date = JsfUtil.getExtremiteJour(new Date());
        set(String.format("%s1", field), date[0]);
        set(String.format("%s1", field), date[1]);
        System.out.println(getField());
    }

    public void prepareSearchByDateBetween(String field, boolean bound) {
        set("_prepareSearchByDateBetween_internal", true);
        prepareSearchByDateBetween("_prepareSearchByDateBetween_internal", field, bound);
    }

    public void prepareSearchByDateBetween(String condition, String field) {
        prepareSearchByDateBetween(condition, field, false);
    }

    public void prepareSearchByDateBetween(String condition, String field, boolean bound) {
        Object date1 = get(String.format("%s1", field));
        Object date2 = get(String.format("%s2", field));

        if (_boolean(condition) && date1 != null && date2 != null) {
            if (bound) {
                date2 = JsfUtil.getExtremiteJour((Date) date2)[1];
            }
            getField().put(field, new Object[]{"between", date1, date2});
        } else {
            getField().remove(field);
        }
        lazyItemList.setRowCount(-1);
    }

    public void unsetOneField(String field) {
        unsetField(field);
    }

    public void unsetField(String... fields) {
        for (String field : fields) {
            getField().remove(field);
        }
    }

    protected String linkTo() {
        return null;
    }

    public void refresh() {
        if (linkTo() == null) {
            itemList = getFacade().builder().fetch(lazyFetch()).where("enabled", true).findAllEntity();
            return;
        }
        T item = ((AbstractController<T>) JsfUtil.getConroller(linkTo() + "Controller")).getSelected();
        if (item != null) {
            itemList = getFacade().builder()
                    .fetch(lazyFetch())
                    .where(linkTo() + ".id", new Object[]{"in", Arrays.asList(item.getId())})
                    .findAllEntity();
        } else {
            itemList = new ArrayList<>();
        }
    }

    protected void afterLoadLasyData() {
    }

    protected void beforeLoadLasyData() {
        if (linkTo() == null) {
            return;
        }
        T item = ((AbstractController<T>) JsfUtil.getConroller(linkTo() + "Controller")).getSelected();
        if (item != null) {
            set(linkTo() + ".id", new Object[]{"in", Arrays.asList(item.getId())});
        } else {
            unsetField(linkTo() + ".id");
        }
    }

    protected void clearList(List... list) {
        for (List l : list) {
            if (l == null) {
                continue;
            }
            l.clear();
        }
    }

    public void validateTag(List<String> list, String pattern, String errorTemplate) {
        if (list == null) {
            return;
        }
        for (String contact : list) {
            if (!contact.matches(pattern)) {
                FacesMessage message = new FacesMessage(String.format(errorTemplate, contact));
                throw new ValidatorException(message);
            }
        }
    }

    public void validateContactTag(FacesContext context, UIComponent componentToValidate, Object value) {
        validateTag((List) value, PATTERN_PHONE, "%s n'est pas un contact valide");
    }

    public void validateEmailTag(FacesContext context, UIComponent componentToValidate, Object value) {
        validateTag((List) value, PATTERN_EMAIL, "%s n'est pas un email valide");
    }

    public class AbstractLazyModel extends LazyDataModel<T> {

        List<T> itemList;
        String[] uniqueField;
        HashMap<String, Object> map;

        public AbstractLazyModel() {
            this.setRowCount(-1);
            map = new HashMap<>();
        }

        public List<T> getItemList() {
            return itemList;
        }

        public T getItemByIndex(int index) {
            try {
                return this.itemList.get(index);
            } catch (Exception ex) {

            }
            return null;
        }

        @Override
        public T getRowData(String rowKey) {
            return (T) getFacade().builder().where("id", Long.parseLong(rowKey)).findOneEntity();
        }

        @Override
        public Object getRowKey(T item) {
            return item.getId();
        }

        @Override
        public List<T> load(int first, int pageSize, String sortField, SortOrder SortOrder,
                Map<String, Object> filters) {

            Boolean sort = null;
            if (sortField != null) {
                if (SortOrder == org.primefaces.model.SortOrder.ASCENDING) {
                    sort = true;
                } else {
                    sort = false;
                }
            }

            map.clear();
            beforeLoadLasyData();
            map.putAll(field);

            if (filters != null) {
                map.putAll(filters);
            }

            Long c = (Long) getFacade().builder().whereMap(map).count("id").findOneTuple().get("count_id");
            this.setRowCount(c.intValue());

            itemList = getFacade().load(first, pageSize, sortField, sort, map, lazyFetch());
            afterLoadLasyData();
            return itemList;
        }

        public void reset() {
            this.setRowCount(-1);
        }
    }

    protected String[] lazyFetch() {
        return new String[]{};
    }

    public List<Integer> listOf(int size) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(i);
        }
        return list;
    }

    public void addNew() {
        prepareCreate();
        getItemList().add(getSelected());
    }

    public void remRow(int index) {
        T c = getItemList().get(index);
        setSelected(c);
        if (c.getId() != null) {
            prepareDelete();
            save();
        } else {
            getItemList().remove(index);
        }
    }

    public void onRowEdit(RowEditEvent event) {
        setSelected((T) event.getObject());
        action = getSelected().getId() != null ? PersistAction.UPDATE : PersistAction.CREATE;
        if (rowCanPersist()) {
            if (action == PersistAction.CREATE) {
                getItemList().remove(getItemList().size() - 1);
            }
            save();
        }
    }

    protected boolean rowCanPersist() {
        return true;
    }

    public void renderRedirectMessage() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> requestParam = context.getExternalContext().getRequestParameterMap();
        String message = requestParam.get("not-allowed-page");
        if (message != null) {
            JsfUtil.addErrorMessage("vous_navez_pas_acces_a_ces_informations");
        }
    }

    public UIComponent findComponent(final String id) {
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot root = context.getViewRoot();
        final UIComponent[] found = new UIComponent[1];
        root.visitTree(new FullVisitContext(context), new VisitCallback() {
            @Override
            public VisitResult visit(VisitContext context, UIComponent component) {
                if (component.getClientId().equals(id)) {
                    found[0] = component;
                    return VisitResult.COMPLETE;
                }
                return VisitResult.ACCEPT;
            }
        });
        return found[0];
    }

    protected void invalidInputValue(String clientId, String errorMessage) {
        FacesContext.getCurrentInstance().validationFailed();
        JsfUtil.msgErreur(clientId, errorMessage);

        try {
            ((EditableValueHolder) findComponent(clientId)).setValid(false);
        } catch (Exception ex) {
        }
    }

    public void download(File file, String contentType, String name) throws IOException {
        download(new FileInputStream(file), contentType, name);
    }

    public void download(InputStream inputStream, String contentType, String name) throws IOException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        response.reset();
        response.setHeader("Content-Type", contentType);

        response.setHeader("Content-Disposition", "attachment;filename=" + name);
        OutputStream responseOutputStream = response.getOutputStream();

        response.setContentLengthLong(inputStream.available());

        byte[] bytesBuffer = new byte[2048];
        int bytesRead;
        while ((bytesRead = inputStream.read(bytesBuffer)) > 0) {
            responseOutputStream.write(bytesBuffer, 0, bytesRead);
        }
        responseOutputStream.flush();
        inputStream.close();
        responseOutputStream.close();

        facesContext.responseComplete();
    }

    public void recountLazyData() {
        lazyItemList.setRowCount(-1);
    }

    public void executeJs(String jsCode) {
        PrimeFaces.current().executeScript(jsCode);
    }

    protected void createActionRepport(String... header) {
        currentReportName = System.nanoTime() + ".csv";
        currentReportFile = myContext.getNewRapportFile(currentReportName);
        closeActionRepport();
        try {
            showReport = false;
            currentReportPs = new PrintStream(currentReportFile);
            writeActionRepport(header);
        } catch (Exception ex) {
            currentReportPs = null;
            currentReportName = null;
        }
    }

    protected void writeActionRepport(String... values) {
        try {
            showReport = true;
            currentReportPs.println(String.join(";", values));
        } catch (Exception ex) {
        }
    }

    protected void closeActionRepport() {
        closeActionRepport(true);
    }

    protected void closeActionRepport(boolean showLink) {
        if (currentReportPs != null) {
            try {
                currentReportPs.flush();
                if (showReport) {
                    currentReportPs.close();
                }
                if (showLink) {
                    showActionReportLink();
                }
            } catch (Exception ex) {
            }
        }
    }

    protected void showActionReportLink() {
        ((AbstractController<BaseEntity>) JsfUtil.getConroller("reportController")).set("action_report_link",
                String.format("/resources/rapport/%s", currentReportName));
        FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add(":NotificationForm");
        executeJs("PF('NotificationBar').show()");
    }

    protected T _chargementEnMasse(int ligne, T entity, String[] datas, boolean newEntty) throws Exception {
        return null;
    }

    protected void _chargementEnMasse(String idField, int indexId, int[] indexRequis, String messageIndexRequis) {
        _chargementEnMasse(idField, indexId, indexRequis, messageIndexRequis, true);
    }

    protected void _chargementEnMasse(String idField, int indexId, int[] indexRequis, String messageIndexRequis,
            boolean persist) {
        try {
            createActionRepport("Ligne", "Log");
            UploadedFileWrapper fileWrapper = (UploadedFileWrapper) get("file");
            StringWriter writer = new StringWriter();
            IOUtils.copy(fileWrapper.getInputstream(), writer, "UTF-8");
            String[] datas = writer.toString().split("\n");
            String[] infos;
            T entity;
            boolean newEntty;
            int ligne = 1;
            importItemList = new ArrayList<T>();
            for (String data : datas) {
                try {
                    if (ligne == 1) {
                        ligne++;
                        continue;
                    }
                    infos = data.split(";");
                    if ("id".equals(idField)) {
                        entity = (T) getFacade().builder().where(idField, Long.parseLong(infos[indexId]))
                                .findOneEntity();
                    } else {
                        entity = (T) getFacade().builder().where(idField, infos[indexId]).findOneEntity();
                    }
                    newEntty = entity == null;
                    if (newEntty) {
                        boolean erreur = false;
                        for (int index : indexRequis) {
                            if (infos[index].trim().isEmpty()) {
                                writeActionRepport(ligne + "", messageIndexRequis);
                                erreur = true;
                                break;
                            }
                        }

                        if (erreur) {
                            ligne++;
                            continue;
                        }
                    }
                    try {
                        T _entity = _chargementEnMasse(ligne, entity, infos, newEntty);
                        if (_entity != null) {
                            entity = _entity;
                        }
                    } catch (ArrayIndexOutOfBoundsException ex) {
                        writeActionRepport(ligne + "", "La ligne semble incomplete");
                    } catch (Exception ex) {
                        writeActionRepport(ligne + "", ex.getMessage());
                        ex.printStackTrace();
                    }
                    if (entity != null) {
                        if (persist) {
                            if (newEntty) {
                                getFacade().create(entity);
                            } else {
                                getFacade().edit(entity);
                            }
                        }
                        importItemList.add(entity);
                    }
                } catch (Exception ex) {
                    writeActionRepport(ligne + "", ex.getMessage());
                    ex.printStackTrace();
                }
                ligne++;
            }
            closeActionRepport();
            set("action", "list");
            lazyItemList.reset();
            JsfUtil.msgSucces("Fichier importée, consulter le rapport pour plus de details.");
        } catch (Exception ex) {

        }
    }

    public void saveImport() {
        for (T entity : importItemList) {
            if (entity.getId() == null) {
                getFacade().create(entity);
            } else {
                getFacade().edit(entity);
            }
        }
        importItemList.clear();
        importItemList = null;
        JsfUtil.msgSucces();
    }

    public String redirectAfterDelay(String page, int delay) {
        if (delay == 0) {
            return redirectTo(page);
        }
        executeJs(String.format("setTimeout(function () {window.location.href = \"%s\"}, %s);", page, delay));
        return null;
    }

    public boolean isPageReloaded() {
        HttpServletRequest origRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        if (!"GET".equals(origRequest.getMethod().toUpperCase())) {
            return false;
        }
        if (FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) {
            return false;
        }
        return true;
    }

    public void _onPageLoad() {

    }

    public void onPageLoad() {
        if (!isPageReloaded()) {
            return;
        }
        if (lazyItemList != null) {
            lazyItemList.reset();
        }
        itemList = null;
        _onPageLoad();
    }

    public Acteur getCurrentActeur() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof Acteur) {
            return (Acteur) authentication.getPrincipal();
        }
        return null;
    }

    public String message(String key) {
        return JsfUtil.getMsg(key);
    }

    public void generatePdf(Map<String, Object> map, String template, File dest) throws Exception {
        Template tpl = freemarkerConfig.getConfiguration().getTemplate(template);
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(tpl, map);
        Document document = new Document(PageSize.A4);
        document.setMargins(10, 10, 10, 10);
        OutputStream outputStream = new FileOutputStream(dest);
        PdfWriter pdfWriter = PdfWriter.getInstance(document, outputStream);
        document.open();
        XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
        worker.parseXHtml(pdfWriter, document, new StringReader(html));
        document.close();
        outputStream.close();
    }

    protected AbstractController getController(String name) {
        return (AbstractController) JsfUtil.getConroller(name);
    }

    public boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    protected void actualiserPage() {
        executeJs("window.location.href='/admin/index.xhtml'");
    }

    public List<T> lastList(int limit) {
        beforeLoadLasyData();
        return getFacade().builder().limit(limit).orderBy("id", QueryBuilder.Order.DESC).findAllEntity();
    }

    protected void onPutField(String key, Object value) {
    }

    ;

    class MyHashMap extends HashMap<String, Object> {

        @Override
        public Object put(String key, Object value) {
            onPutField(key, value);
            return super.put(key, value);
        }
    }
}
