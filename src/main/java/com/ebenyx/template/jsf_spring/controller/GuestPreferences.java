package com.ebenyx.template.jsf_spring.controller;

import com.ebenyx.template.jsf_spring.JsfUtil;
import com.ebenyx.template.jsf_spring.facade.ParametreFacade;
import com.ebenyx.template.jsf_spring.model.Parametre;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;


@ManagedBean
@ApplicationScoped
public class GuestPreferences implements Serializable {
    
    @Autowired
    ParametreFacade parametreFacade;
    
    Parametre parametre;
    private List<ComponentTheme> componentThemes = new ArrayList<ComponentTheme>();
    
    @PostConstruct
    public void init() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        ServletContext servletContext = (ServletContext) externalContext.getContext();
	WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext).
            getAutowireCapableBeanFactory().
            autowireBean(this);
        
        
        componentThemes.add(new ComponentTheme("Amber Accent", "amber-accent", "amber-accent.svg"));
        componentThemes.add(new ComponentTheme("Amber Light", "amber-light", "amber-light.svg"));
        componentThemes.add(new ComponentTheme("Amber Dark", "amber-dark", "amber-dark.svg"));
        componentThemes.add(new ComponentTheme("Blue Accent", "blue-accent", "blue-accent.svg"));
        componentThemes.add(new ComponentTheme("Blue Light", "blue-light", "blue-light.svg"));
        componentThemes.add(new ComponentTheme("Blue Dark", "blue-dark", "blue-dark.svg"));
        componentThemes.add(new ComponentTheme("Blue Grey Accent", "bluegrey-accent", "bluegrey-accent.svg"));
        componentThemes.add(new ComponentTheme("Blue Grey Light", "bluegrey-light", "bluegrey-light.svg"));
        componentThemes.add(new ComponentTheme("Blue Grey Dark", "bluegrey-dark", "bluegrey-dark.svg"));
        componentThemes.add(new ComponentTheme("Brown Accent", "brown-accent", "brown-accent.svg"));
        componentThemes.add(new ComponentTheme("Brown Light", "brown-light", "brown-light.svg"));
        componentThemes.add(new ComponentTheme("Brown Dark", "brown-dark", "brown-dark.svg"));
        componentThemes.add(new ComponentTheme("Cyan Accent", "cyan-accent", "cyan-accent.svg"));
        componentThemes.add(new ComponentTheme("Cyan Light", "cyan-light", "cyan-light.svg"));
        componentThemes.add(new ComponentTheme("Cyan Dark", "cyan-dark", "cyan-dark.svg"));
        componentThemes.add(new ComponentTheme("Deep Orange Accent", "deeporange-accent", "deeporange-accent.svg"));
        componentThemes.add(new ComponentTheme("Deep Orange Light", "deeporange-light", "deeporange-light.svg"));
        componentThemes.add(new ComponentTheme("Deep Orange Dark", "deeporange-dark", "deeporange-dark.svg"));
        componentThemes.add(new ComponentTheme("Deep Purple Accent", "deeppurple-accent", "deeppurple-accent.svg"));
        componentThemes.add(new ComponentTheme("Deep Purple Light", "deeppurple-light", "deeppurple-light.svg"));
        componentThemes.add(new ComponentTheme("Deep Purple Dark", "deeppurple-dark", "deeppurple-dark.svg"));
        componentThemes.add(new ComponentTheme("Green Accent", "green-accent", "green-accent.svg"));
        componentThemes.add(new ComponentTheme("Green Light", "green-light", "green-light.svg"));
        componentThemes.add(new ComponentTheme("Green Dark", "green-dark", "green-dark.svg"));
        componentThemes.add(new ComponentTheme("Indigo Accent", "indigo-accent", "indigo-accent.svg"));
        componentThemes.add(new ComponentTheme("Indigo Light", "indigo-light", "indigo-light.svg"));
        componentThemes.add(new ComponentTheme("Indigo Dark", "indigo-dark", "indigo-dark.svg"));
        componentThemes.add(new ComponentTheme("Light Blue Accent", "lightblue-accent", "lightblue-accent.svg"));
        componentThemes.add(new ComponentTheme("Light Blue Light", "lightblue-light", "lightblue-light.svg"));
        componentThemes.add(new ComponentTheme("Light Blue  Dark", "lightblue-dark", "lightblue-dark.svg"));
        componentThemes.add(new ComponentTheme("Light Green Accent", "lightgreen-accent", "lightgreen-accent.svg"));
        componentThemes.add(new ComponentTheme("Light Green Light", "lightgreen-light", "lightgreen-light.svg"));
        componentThemes.add(new ComponentTheme("Light Green  Dark", "lightgreen-dark", "lightgreen-dark.svg"));
        componentThemes.add(new ComponentTheme("Lime Accent", "lime-accent", "lime-accent.svg"));
        componentThemes.add(new ComponentTheme("Lime Light", "lime-light", "lime-light.svg"));
        componentThemes.add(new ComponentTheme("Lime Dark", "lime-dark", "lime-dark.svg"));
        componentThemes.add(new ComponentTheme("Orange Accent", "orange-accent", "orange-accent.svg"));
        componentThemes.add(new ComponentTheme("Orange Light", "orange-light", "orange-light.svg"));
        componentThemes.add(new ComponentTheme("Orange Dark", "orange-dark", "orange-dark.svg"));
        componentThemes.add(new ComponentTheme("Pink Accent", "pink-accent", "pink-accent.svg"));
        componentThemes.add(new ComponentTheme("Pink Light", "pink-light", "pink-light.svg"));
        componentThemes.add(new ComponentTheme("Pink Dark", "pink-dark", "pink-dark.svg"));
        componentThemes.add(new ComponentTheme("Purple Accent", "purple-accent", "purple-accent.svg"));
        componentThemes.add(new ComponentTheme("Purple Light", "purple-light", "purple-light.svg"));
        componentThemes.add(new ComponentTheme("Purple Dark", "purple-dark", "purple-dark.svg"));
        componentThemes.add(new ComponentTheme("Teal Accent", "teal-accent", "teal-accent.svg"));
        componentThemes.add(new ComponentTheme("Teal Light", "teal-light", "teal-light.svg"));
        componentThemes.add(new ComponentTheme("Teal Dark", "teal-dark", "teal-dark.svg"));
        componentThemes.add(new ComponentTheme("Yellow Accent", "yellow-accent", "yellow-accent.svg"));
        componentThemes.add(new ComponentTheme("Yellow Light", "yellow-light", "yellow-light.svg"));
        componentThemes.add(new ComponentTheme("Yellow Dark", "yellow-dark", "yellow-dark.svg"));
        
        parametre = parametreFacade.get();
    }

    public String getLayout() {
        return parametre.getLayout();
    }

    public String getTheme() {
        return parametre.getTheme();
    }

    public void setTheme(String theme) {
        parametre.setTheme(theme);
        parametre.setLayout(parametre.getTheme().split("-")[0]);
        parametre.setDarkLogo(parametre.getLayout().equals("lime") || parametre.getLayout().equals("yellow"));
    }

    public void changeTheme(String theme, boolean dark) {
        parametre.setTheme(theme);
        parametre.setDarkMenu(dark);
    }

    public String getMenuMode() {
        return parametre.getMenuMode();
    }

    public void setMenuMode(String menuMode) {
        parametre.setMenuMode(menuMode);

        if (parametre.getMenuMode().equals("layout-horizontal")) {
            parametre.setGroupedMenu(true);
        }
    }

    public boolean getDarkMenu() {
        return parametre.getDarkMenu();
    }

    public void setDarkMenu(boolean darkMenu) {
        parametre.setDarkMenu(darkMenu);
    }

    public String getMenuColor() {
        return parametre.getDarkMenu() ? "layout-menu-dark" : "layout-menu-light";
    }

    public String getProfileMode() {
        return parametre.getProfileMode();
    }

    public void setProfileMode(String profileMode) {
        parametre.setProfileMode(profileMode);
    }

    public boolean getGroupedMenu() {
        return parametre.getGroupedMenu();
    }

    public void setGroupedMenu(boolean value) {
        parametre.setGroupedMenu(value);
        parametre.setMenuMode("layout-static");
    }

    public boolean isDarkLogo() {
        return parametre.getDarkLogo();
    }

    public boolean isOrientationRTL() {
        return parametre.getOrientationRTL();
    }

    public void setOrientationRTL(boolean orientationRTL) {
        parametre.setOrientationRTL(orientationRTL);
    }

    public List<ComponentTheme> getComponentThemes() {
        return componentThemes;
    }

    public void setComponentThemes(List<ComponentTheme> componentThemes) {
        this.componentThemes = componentThemes;
    }
    public void saveParam(){
        parametreFacade.edit(parametre);
        JsfUtil.msgSucces();
    }
    
    public class ComponentTheme {
        String name;
        String file;
        String image;

        public ComponentTheme(String name, String file, String image) {
            this.name = name;
            this.file = file;
            this.image = image;
        }

        public String getName() {
            return this.name;
        }

        public String getFile() {
            return this.file;
        }

        public String getImage() {
            return this.image;
        }
    }
}
