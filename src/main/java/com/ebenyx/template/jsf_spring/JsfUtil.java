package com.ebenyx.template.jsf_spring;

import com.google.gson.Gson;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.servlet.http.HttpSession;
import org.primefaces.model.UploadedFile;
import javax.servlet.http.HttpServletRequest;

public class JsfUtil {
	
    public static final Gson gson = new Gson();
        
	private static final ResourceBundle BUNDLE = FacesContext.getCurrentInstance().getApplication()
			.getResourceBundle(FacesContext.getCurrentInstance(), "bundle_fr");

	public static final SimpleDateFormat f_analyse = new SimpleDateFormat("yyMMddhhmm");
	public static final SimpleDateFormat f_ddMMMyyyyhhmm = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
	public static final SimpleDateFormat f_hhmm = new SimpleDateFormat("hh:mm");
	public static final SimpleDateFormat f_ddMMMyyyy = new SimpleDateFormat("dd-MMM-yyyy");
	public static final SimpleDateFormat ddMMyyyy = new SimpleDateFormat("dd-MM-yyyy");
	public static final SimpleDateFormat f_yyyy = new SimpleDateFormat("yyyy");
	public static DateFormat dateformat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());

	public static boolean isValidationFailed() {
		return FacesContext.getCurrentInstance().isValidationFailed();
	}

	public static void msgErreur(String id, String key, Object... param) {
		String msg = getMsg(key, param);
		FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
		FacesContext.getCurrentInstance().addMessage(id, facesMsg);
	}

	public static void msgErreur(String key, Object... param) {
		msgErreur(null, key, param);
		FacesContext.getCurrentInstance().validationFailed();
	}

	public static void msgErreurListeVide() {
		msgErreur("la_liste_concernee_est_vide");
	}

	public static void msgErreurVeuillezSelectionnerUnElement() {
		msgErreur("veuillez_selectionnez_un_element");
	}

	public static void msgErreurSaisieIncoherente() {
		JsfUtil.msgErreur("les_informations_saisies_semblent_incoherentes");
	}

	public static void msgSucces() {
		msgSucces("traitement_effectue_avec_succes");
	}

	public static void msgSuccesCreate() {
		msgSucces("insertion_effectuee_avec_succes");
	}

	public static void msgSuccesUpdate() {
		msgSucces("mise_a_jour_effectuee_avec_succes");
	}

	public static void msgSuccesRemove() {
		msgSucces("suppression_effectuee_avec_succes");
	}

	/// A SUPPRIMER + TARD
	public static void addErrorMessage(String msg) {
		JsfUtil.msgErreur(msg);
	}

	public static void addSuccessMessage(String msg) {
		JsfUtil.msgSucces(msg);
	}

	public static void addErrorMessage(Exception ex, String msg) {
		JsfUtil.msgSucces(msg);
	}
	/// FIN A SUPRIEMR +TARD

	public static void msgElementDejaEnList() {
		msgSucces("element_deja_en_liste");
	}

	public static void msgSucces(String key, Object... params) {
		String msg = getMsg(key, params);
		FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
		FacesContext.getCurrentInstance().addMessage(null, facesMsg);
	}

	public static void addWarnMessage(String text) {
		FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_WARN, text, text);
		FacesContext.getCurrentInstance().addMessage(null, facesMsg);
	}

	public static String getMsg(String key, Object... params) {
		try {
			if (BUNDLE.containsKey(key)) {
				return MessageFormat.format(BUNDLE.getString(key), params);
			}
		} catch (Exception e) {
		}
		return key;
	}

	public static String getRequestParameter(String key) {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
	}

	public static Object getObjectFromRequestParameter(String requestParameterName, Converter converter,
			UIComponent component) {
		String theId = JsfUtil.getRequestParameter(requestParameterName);
		return converter.getAsObject(FacesContext.getCurrentInstance(), component, theId);
	}

	public static double getDouble(Object o) {
		if (o == null) {
			return 0;
		}
		return Double.parseDouble("0" + o);
	}

	public static int getInt(Object o) {
		return (int) getDouble(o);
	}

	public static double doublePrecision(double val) {
		return Double.parseDouble(String.format("%." + 3 + "f", val).replace(',', '.'));
	}

	public static String formatDouble(double val) {
		return formatDouble(val, 3);
	}

	public static String formatDouble(double val, int precision) {
		return String.format("%." + precision + "f", val);
	}

	public static Object getConroller(final String beanName) {
		Object bean;
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			bean = context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", Object.class);
		} catch (Exception e) {
			throw new FacesException(e.getMessage(), e);
		}
		if (bean == null) {
			throw new FacesException("Managed bean with name '" + beanName + "' was not found.");
		}
		return bean;
	}
	/*
	 * public static User getUser() { return getMainConroller().getUser(); }
	 * 
	 * public static MainController getMainConroller() { return (MainController)
	 * getConroller("mainController"); }
	 */

	public static String crypt(String text) {
		String crypt = "";
		if (text == null) {
			return null;
		}

		int c;
		for (int i = 0; i < text.length(); i++) {
			c = text.charAt(i) ^ 23;
			crypt = crypt + (char) c;
		}
		return crypt;
	}

	public static int getJourEntreDate(Date debut, Date fin) {
		if (debut == null || fin == null) {
			return 0;
		}

		long diff = fin.getTime() - debut.getTime();
		return (int) (diff / 86400000);

	}

	public static long getWorkingDaysBetween(Date start, Date end) {
		// Ignore argument check
		Calendar c1 = Calendar.getInstance();
		c1.setTime(start);
		int w1 = c1.get(Calendar.DAY_OF_WEEK);
		c1.add(Calendar.DAY_OF_WEEK, -w1);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(end);
		int w2 = c2.get(Calendar.DAY_OF_WEEK);
		c2.add(Calendar.DAY_OF_WEEK, -w2);

		// end Saturday to start Saturday
		long days = (c2.getTimeInMillis() - c1.getTimeInMillis()) / (1000 * 60 * 60 * 24);
		long daysWithoutWeekendDays = days - (days * 2 / 7);

		// Adjust days to add on (w2) and days to subtract (w1) so that Saturday
		// and Sunday are not included
		if (w1 == Calendar.SUNDAY && w2 != Calendar.SATURDAY) {
			w1 = Calendar.MONDAY;
		} else if (w1 == Calendar.SATURDAY && w2 != Calendar.SUNDAY) {
			w1 = Calendar.FRIDAY;
		}

		if (w2 == Calendar.SUNDAY) {
			w2 = Calendar.MONDAY;
		} else if (w2 == Calendar.SATURDAY) {
			w2 = Calendar.FRIDAY;
		}

		return daysWithoutWeekendDays - w1 + w2;
	}

	public static double max(double... val) {
		Double maximum = null;
		for (double v : val) {
			if (maximum == null || maximum < v) {
				maximum = v;
			}
		}
		return maximum;
	}

	public static double min(double... val) {
		Double minimum = null;
		for (double v : val) {
			if (minimum == null || minimum > v) {
				minimum = v;
			}
		}
		return minimum;
	}

	public static void clearList(List[] lists) {
		for (List list : lists) {
			if (list != null) {
				list.clear();
			}
		}
	}

	public static void traitementException(Exception ex, String className) {
		if (ex instanceof EJBException) {
			String msg = "";
			Throwable cause = ex.getCause();
			if (cause != null) {
				msg = cause.getLocalizedMessage();
			}
			if (msg.length() > 0) {
				JsfUtil.msgErreur(msg);
			} else {
				JsfUtil.msgErreur("erreur_rencontree_lors_du_traitement_des_informations");
			}
		} else {
			Logger.getLogger(className).log(Level.SEVERE, null, ex);
			JsfUtil.msgErreur("erreur_rencontree_lors_du_traitement_des_informations");
		}
	}

	public static long[] getHeureMinute(long minutes) {
		long[] hm = new long[2];
		hm[0] = minutes / 60;
		hm[1] = minutes % 60;
		return hm;
	}

	public static long getMinutes(Date debut, Date fin) {
		long diffMs = fin.getTime() - debut.getTime();
		long diffSec = diffMs / 1000;
		return diffSec / 60;
	}

	public static Date[] getExtremiteSemaine(Date jour) {
		Calendar date = Calendar.getInstance();
		date.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		Date date1 = date.getTime();

		date.add(Calendar.DATE, 7);
		date.add(Calendar.SECOND, -1);
		Date date2 = date.getTime();
		return new Date[] { date1, date2 };

	}

	public static Date[] getExtremiteJour(Date jour) {
		Calendar date = Calendar.getInstance();
		date.setTime(jour);

		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		Date date1 = date.getTime();

		date.set(Calendar.HOUR_OF_DAY, 23);
		date.set(Calendar.MINUTE, 59);
		date.set(Calendar.SECOND, 59);
		Date date2 = date.getTime();
		return new Date[] { date1, date2 };

	}

	public static Date[] getExtremiteMois(Date jour) {
		Calendar date = Calendar.getInstance();
		date.setTime(jour);

		date.set(Calendar.DAY_OF_MONTH, 1);
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		Date date1 = date.getTime();
		/*
		 * date.set(Calendar.DAY_OF_MONTH, date.getMaximum(Calendar.DAY_OF_MONTH));
		 * date.set(Calendar.HOUR_OF_DAY, 23); date.set(Calendar.MINUTE, 59);
		 * date.set(Calendar.SECOND, 59);
		 */
		date.add(Calendar.MONTH, 1);
		date.add(Calendar.SECOND, -1);
		Date date2 = date.getTime();
		return new Date[] { date1, date2 };

	}

	public static String ago(Date startDate) {
		return ago(startDate, new Date());
	}

	public static String ago(Date startDate, Date endDate) {
		long different = endDate.getTime() - startDate.getTime();

		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;
		long daysInMilli = hoursInMilli * 24;

		long elapsedDays = different / daysInMilli;
		different = different % daysInMilli;

		long elapsedHours = different / hoursInMilli;
		different = different % hoursInMilli;

		long elapsedMinutes = different / minutesInMilli;
		different = different % minutesInMilli;

		// long elapsedSeconds = different / secondsInMilli;
		if (elapsedDays > 0) {
			return String.format("%s jour(s), %s heure(s)", elapsedDays, elapsedHours);
		} else if (elapsedHours > 0) {

			return String.format("%s heure(s), %s minutes", elapsedHours, elapsedMinutes);
		} else if (elapsedMinutes > 0) {
			return String.format("%s minute(s)", elapsedMinutes);

		}
		return "a l'instant";
	}

	public static String getHeureMinuteString(long minutes) {
		long[] heure_mn = getHeureMinute(minutes);
		return heure_mn[0] + "H " + heure_mn[1] + "Mn";
	}

	static public Object from_to(List list_from, List list_to, int ligne) {
		return from_to(list_from, list_to, ligne, true);
	}

	static public Object from_to(List list_from, List list_to, int ligne, boolean remove_from_source) {
		Object elt = null;
		if (list_to != null) {
			if (!list_to.contains(list_from.get(ligne))) {
				list_to.add(list_from.get(ligne));
				elt = list_from.get(ligne);
			}
		}
		if (remove_from_source) {
			list_from.remove(ligne);
		}
		return elt;
	}

	public static byte[] getSalt() {
		try {
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
			byte[] salt = new byte[16];
			sr.nextBytes(salt);
			return salt;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getSecurePassword(String passwordToHash, byte[] salt) {
		String generatedPassword = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(salt);
			byte[] bytes = md.digest(passwordToHash.getBytes());
			// Convert it to hexadecimal format
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}

	public static String getSessionId() {
		FacesContext fCtx = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		return session.getId();
	}
	/*
	 * public static void createPDF(String url,String fileName) { FacesContext
	 * facesContext = FacesContext.getCurrentInstance(); ExternalContext
	 * externalContext = facesContext.getExternalContext(); try { ITextRenderer
	 * renderer = new ITextRenderer(); renderer.setDocument(new
	 * URL(url).toString()); renderer.layout(); HttpServletResponse response =
	 * (HttpServletResponse) externalContext.getResponse(); response.reset();
	 * response.setContentType("application/pdf");
	 * response.setHeader("Content-Disposition", "inline; filename=\"" + fileName +
	 * "\""); OutputStream browserStream = response.getOutputStream();
	 * renderer.createPDF(browserStream); facesContext.responseComplete(); } catch
	 * (Exception ex) { Logger.getLogger(JsfUtil.class.getName()).log(Level.SEVERE,
	 * null, ex); } }
	 */

	/*
	 * public void htmlStringToPdfStream(String html, String tempFile) { try {
	 * FileOutputStream pdf = new FileOutputStream(tempFile);
	 * 
	 * ITextRenderer renderer = new ITextRenderer();
	 * renderer.setDocumentFromString(html); renderer.layout();
	 * renderer.createPDF(pdf); pdf.close(); } catch (Exception e) { throw new
	 * RuntimeException(e); } }
	 */
	public static void removeFile(String filePath) {
		File f = new File(filePath);
		f.delete();
	}

	/*
	 * public static void importCSV(AbstractImportedObjectBuilder builder,
	 * UploadedFile csvFile) { importCSV(builder, csvFile, ";"); }
	 * 
	 * public static void importCSV(AbstractImportedObjectBuilder builder,
	 * UploadedFile csvFile, String csvSplitBy) { BufferedReader br = null; String
	 * line; try {
	 * 
	 * br = new BufferedReader(new InputStreamReader(csvFile.getInputstream()));
	 * while ((line = br.readLine()) != null) { builder.add(line.split(csvSplitBy));
	 * } } catch (Exception e) { e.printStackTrace(); } finally { if (br != null) {
	 * try { br.close(); } catch (IOException e) { e.printStackTrace(); } } } }
	 */
	public static String getFilejsfbaseappName(String fileName) {
		return fileName.substring(0, fileName.lastIndexOf(".") + 1);
	}

	public static String getFileExtension(String filename) {
		try {
			return filename.substring(filename.lastIndexOf(".") + 1);
		} catch (Exception e) {
			return "";
		}
	}

	public static boolean save_file(UploadedFile uploadedFile, String folder, Object id) {
		return save_file(uploadedFile, folder + id + "." + JsfUtil.getFileExtension(uploadedFile.getFileName()));
	}

	public static boolean save_file(UploadedFile uploadedFile, String filePath) {
		try {
			File file = new File(filePath);
			if (file.getParentFile().exists() == false) {
				file.getParentFile().mkdirs();
			}
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
			copyFile(uploadedFile.getInputstream(), stream);
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	public static void copyFile(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int length;
		while ((length = in.read(buffer)) > 0) {
			out.write(buffer, 0, length);
		}
		in.close();
		out.close();
	}

	public static Date[] getExtremiteAnnee(Date mois) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(mois);
		Date debut, fin;
		gc.set(Calendar.MONTH, Calendar.JANUARY);
		gc.set(Calendar.DAY_OF_MONTH, 1);
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		debut = gc.getTime();

		gc.set(Calendar.MONTH, Calendar.DECEMBER);
		gc.set(Calendar.DAY_OF_MONTH, gc.getMaximum(Calendar.DAY_OF_MONTH));
		gc.set(Calendar.HOUR_OF_DAY, 23);
		gc.set(Calendar.MINUTE, 59);
		gc.set(Calendar.SECOND, 59);
		fin = gc.getTime();
		return new Date[] { debut, fin };
	}
	/*
	 * public static boolean can(String actions) { return
	 * JsfUtil.getUser().can(actions); }
	 */

	public static String evalAsString(String p_expression) {
		FacesContext context = FacesContext.getCurrentInstance();
		ExpressionFactory expressionFactory = context.getApplication().getExpressionFactory();
		ELContext elContext = context.getELContext();
		ValueExpression vex = expressionFactory.createValueExpression(elContext, p_expression, String.class);
		String result = (String) vex.getValue(elContext);
		return result;
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}

	public static byte[] convertFileToByteArray(String fileName) {

		InputStream inputStream = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			inputStream = new FileInputStream(fileName);
			byte[] buffer = new byte[1024];
			baos = new ByteArrayOutputStream();
			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return baos.toByteArray();
	}

	public static Object getFieldValue(Object obj, String fieldName)
			throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		Field field = obj.getClass().getDeclaredField(fieldName);
		field.setAccessible(true);
		return field.get(obj);
	}

	public static Object getFieldValueString(Object obj, String fieldName)
			throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		Object value = getFieldValue(obj, fieldName);
		if (value instanceof Date) {
			value = ddMMyyyy.format(value);
		}
		return value;
	}

}
