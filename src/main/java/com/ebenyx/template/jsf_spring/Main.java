/**
 *  (C) 2013-2014 Stephan Rauh http://www.beyondjava.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ebenyx.template.jsf_spring;

import com.ebenyx.template.jsf_spring.facade.ParametreFacade;
import com.ebenyx.template.jsf_spring.facade.ActeurFacade;
import com.ebenyx.template.jsf_spring.facade.DatabaseServiceFacade;
import com.ebenyx.template.jsf_spring.facade.RoleFacade;
import com.ebenyx.template.jsf_spring.model.Parametre;
import com.ebenyx.template.jsf_spring.model.Role;
import com.ebenyx.template.jsf_spring.model.Acteur;
import com.sun.faces.config.ConfigureListener;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.Executor;
import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.context.ServletContextAware;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;

import com.ebenyx.template.jsf_spring.model.DatabaseService;

import java.math.BigInteger;
import org.springframework.scheduling.annotation.Async;

@EnableAsync
@EnableWebSecurity
@EnableScheduling
@EnableJpaRepositories
@SpringBootApplication
@EnableConfigurationProperties({ApplicationProperties.class})
public class Main extends WebSecurityConfigurerAdapter implements ServletContextAware {

    final static private org.slf4j.Logger logger = LoggerFactory.getLogger(Main.class);

    @Autowired
    private CustomAuthenticationProvider authProvider;

    @Autowired
    private ActeurFacade acteurFacade;

    @Autowired
    private RoleFacade roleFacade;

    @Autowired
    private ParametreFacade parametreFacade;
    
    //@Autowired
    //private transient DatabaseServiceFacade databaseFacade;

    @Autowired
    private MyContext myContext;

    private static String PID;

    public static void main(String[] args) throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        if (args.length > 0) {
            if ("service".equals(args[0])) {
                runAsService();
                return;
            }

            logger.info("arguments  length " + args.length);
            killApp(args[0]);
        }

        SpringApplication springApplication = new SpringApplication(Main.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);
        setupProcessId();
    }

    public static void runAsService() {
        try {
            String cmd = "java -jar ./target/sgci_avis_debit_credit-1.0.jar";
            System.out.println(cmd);
            Runtime.getRuntime().exec(cmd);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void setupProcessId() {
        try {
            File file = new File("./application.pid");
            BufferedReader br = new BufferedReader(new FileReader(file));
            PID = br.readLine();
            br.close();
            logger.info("PID " + PID);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void killApp(String pid) {
        try {
            String cmd = String.format("kill -9 %s", pid);
            logger.info(cmd);
            Runtime.getRuntime().exec(cmd);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void restart() {
        try {
            String cmd = "java -jar ./target/sgci_avis_debit_credit-1.0.jar " + PID;
            logger.info(cmd);
            Runtime.getRuntime().exec(cmd);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Bean
    public ServletRegistrationBean facesServletRegistration() {
        FacesServlet facesServlet = new FacesServlet();
        ServletRegistrationBean registration = new ServletRegistrationBean(
                facesServlet, "*.xhtml");
        registration.setName("FacesServlet");
        registration.setLoadOnStartup(1);
        return registration;
    }

    @Bean
    public MyContext myContext() {
        return new MyContext();
    }

    @Bean
    public ServletListenerRegistrationBean<ConfigureListener> jsfConfigureListener() {
        return new ServletListenerRegistrationBean<ConfigureListener>(new ConfigureListener());
    }

    @Bean
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(50);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("jsf_spring-");
        executor.initialize();
        return executor;
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new org.primefaces.webapp.filter.FileUploadFilter());
        registration.addServletNames("FacesServlet");
        registration.addInitParameter("thresholdSize", "10240000");
        registration.setName("PrimeFaces FileUpload Filter");
        registration.setOrder(1);
        registration.setEnabled(true);
        return registration;
    }

    @Bean
    public CustomAuthenticationHandler customAuthenticationHandler() {
        return new CustomAuthenticationHandler();
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        servletContext.setInitParameter("com.sun.faces.forceLoadConfiguration", Boolean.TRUE.toString());
        servletContext.setInitParameter("primefaces.THEME", "babylon-blue-accent");
        //servletContext.setInitParameter("primefaces.THEME", "none");
        servletContext.setInitParameter("primefaces.FONT_AWESOME", Boolean.TRUE.toString());
        servletContext.setInitParameter("javax.faces.STATE_SAVING_METHOD", "server");
        servletContext.setInitParameter("javax.faces.PROJECT_STAGE", "Development");
        //servletContext.setInitParameter("javax.faces.PROJECT_STAGE", "Production");
        servletContext.setInitParameter("javax.faces.FACELETS_SKIP_COMMENTS", "false");
        servletContext.setInitParameter("com.sun.faces.expressionFactory", "com.sun.el.ExpressionFactoryImpl");
        servletContext.setInitParameter("primefaces.UPLOADER", "commons");
    }

    /**
     * Configuration du mecanisme de securite de l'application
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(
                        "/",
                        "/logout",
                        "/login",
                        "/api/1/test",
                        "/api/1/connexion",
                        "/api/1/*",
                        "/creer_mdp.xhtml",
                        "/mdp_oublie.xhtml",
                        "/login.xhtml",
                        "/reponse/*"
                ).permitAll() // l'acces a ces url necessite d'etre connecter
                .anyRequest().authenticated()
                .and()
                .csrf()
                .disable()
                .formLogin()
                .loginPage("/login.xhtml") // page de connexion
                .loginProcessingUrl("/login") //
                .failureHandler(customAuthenticationHandler()) // 
                .successHandler(customAuthenticationHandler()) // 
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout")) // Lien de déconnexion
                .logoutSuccessUrl("/login.xhtml") // page de redirection apres la deconnexion
                .and()
                .exceptionHandling()
                .accessDeniedPage("/access.xhtml") // Page acces non autorisé
                .and()
                .authenticationProvider(authProvider);

        // les données a chargement au demarrage de l'application peuvent se faire a ce niveau
        initData();
    }

    @Bean
    public JavaMailSender getJavaMailSender() {
        Parametre parametre = parametreFacade.get();
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        parametre.setup(mailSender);
        return mailSender;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers(
                        "/javax.faces.resource/**",
                        "/favicon.ico",
                        "/resources/**",
                        "/static/**",
                        "/css/**",
                        "/js/**",
                        "/images/**"
                );
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }

    // load initial data
    private void initData() {
        createDefaultRole();
        createDefaultUser();
        // createDefaulDb();

        //_do();
    }

    private void createDefaultRole() {
        //creation d'un role par defaut
        try {
            File file = new File(String.format("%s/resources/bundle_fr.properties", new File(myContext.getBasePath()).getParentFile().getAbsolutePath()));
            Properties properties = new Properties();
            properties.load(new FileInputStream(file));
            Role role;
            TreeSet<String> roleSet = new TreeSet<>();
            for (Object key : properties.keySet()) {
                if (key.toString().startsWith("role_")) {
                    try {
                        roleSet.add(key.toString());
                        role = new Role(key.toString());
                        roleFacade.create(role);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            List<Role> roleList = roleFacade.builder().findAllEntity();
            for (Role r : roleList) {
                if (roleSet.contains(r.getLabel())) {
                    continue;
                }
                roleFacade.transaction(() -> {
                    roleFacade.getEM().createNativeQuery("delete from user_role where role_id =" + r.getId()).executeUpdate();
                    roleFacade.remove(r);
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void createDefaultUser() {
        // creation des utilisateurs par defaut
        List<Acteur> acteurs = acteurFacade.builder().where(
                ".or",
                new Object[]{
                    "roles.label", "role_admin",
                    "roles.label", "role_ADMIN"
                }
        ).findAllEntity();

        // Si la liste d'acteurs n'est pas vide alors...
        if (!acteurs.isEmpty()) {
            acteurs.get(0).setRoles(roleFacade.builder().findAllEntity());
            acteurFacade.edit(acteurs.get(0));
            return;
        }

        acteurFacade.create(Acteur.admin(roleFacade.builder().findAllEntity()));
        acteurFacade.create(Acteur.agent_carte(roleFacade.getBy("label", Role.ROLE_AGENT_RETRAIT_CARTE)));
        acteurFacade.create(Acteur.agent_code(roleFacade.getBy("label", Role.ROLE_AGENT_RETRAIT_CODE)));
    }
    
    private void createDefaulDb() {
        //databaseFacade.create(DatabaseService.db_pgsql());
        //databaseFacade.create(DatabaseService.db_mysql());
        //databaseFacade.create(DatabaseService.db_orable());
        //databaseFacade.create(DatabaseService.db_sqlserver());
    }

    @Async
    public static void _do() {

        // 192.168.8.118:8080
        String user = "admin";
        String pass = "admin";

        String url = "http://192.168.8.118:8080/alfresco/api/-default-/public/cmis/versions/1.1/atom";

        // path = Sites/dossier-client/numero_client
        String mFolder = "Sites/dossier-client/123456789";

        // implémentation d'usine par défaut
        SessionFactory factory = SessionFactoryImpl.newInstance();
        Map<String, String> parameter = new HashMap<String, String>();

        SessionParameter sp = null;

        // informations d'identification de l'utilisateur
        parameter.put(SessionParameter.USER, user);
        parameter.put(SessionParameter.PASSWORD, pass);

        // paramètres de connexion
        parameter.put(SessionParameter.ATOMPUB_URL, url);
        parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());

        // créer une session
        Session session = factory.getRepositories(parameter).get(0)
                .createSession();
        Folder root = session.getRootFolder(); // root = "/"

        // properties
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
        properties.put(PropertyIds.NAME, mFolder); // nom de dossier
        
        Folder folder = (Folder) session.getObjectByPath(root.getPath() + mFolder);
        System.out.println("");
        
        // Si le dossier n'existe pas
        if (session.existsPath(root.getPath() + mFolder)) {
            // créer le dossier
            System.out.println("--- session.existsPath ----- " + session.existsPath(root.getPath() + mFolder));
            folder = root.createFolder(properties);
        } else {
        }

        String name = "NewTextFil_2.txt";

        // properties
        Map<String, Object> docProperties = new HashMap<String, Object>();
        docProperties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
        docProperties.put(PropertyIds.NAME, name);

        // contenu
        byte[] content = "Hello Codeur Ebenyx ...!".getBytes();
        InputStream stream = new ByteArrayInputStream(content);
        ContentStream contentStream = new ContentStreamImpl(name,
                BigInteger.valueOf(content.length), "text/plain", stream);

        // créer une version majeure
        Document newDoc = folder.createDocument(docProperties, contentStream,
                VersioningState.MAJOR);

        System.out.println("DONE.");
    }
}
