package com.ebenyx.template.jsf_spring.converter;

import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.DatabaseServiceFacade;
import com.ebenyx.template.jsf_spring.model.DatabaseService;
import javax.faces.convert.FacesConverter;
import org.springframework.beans.factory.annotation.Autowired;

public class DatabaseServiceConverter{
    
    @FacesConverter(forClass = DatabaseService.class)
    public static class ConverterByClass extends AbstractConverter<DatabaseService> {
        @Autowired
        DatabaseServiceFacade facade;
        public ConverterByClass() {
            super(DatabaseService.class);
        }

        @Override
        public AbstractFacade<DatabaseService> getFacade() {
            return facade;
        }
    }    
    @FacesConverter(value ="DatabaseServiceConverter")
    public static class ConverterByName extends AbstractConverter<DatabaseService> {
        @Autowired
        DatabaseServiceFacade facade;

        public ConverterByName() {
            super(DatabaseService.class);
        }

        @Override
        public AbstractFacade<DatabaseService> getFacade() {
            return null;
        }
    }
}
    
        