package com.ebenyx.template.jsf_spring.converter;

import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.ServiceFacade;
import com.ebenyx.template.jsf_spring.model.Service;
import javax.faces.convert.FacesConverter;
import org.springframework.beans.factory.annotation.Autowired;

public class ServiceConverter{
    @FacesConverter(forClass = Service.class)
    public static class ConverterByClass extends AbstractConverter<Service> {
        @Autowired
        ServiceFacade facade;
        public ConverterByClass() {
            super(Service.class);
        }

        @Override
        public AbstractFacade<Service> getFacade() {
            return facade;
        }
    }    
    @FacesConverter(value ="ServiceConverter")
    public static class ConverterByName extends AbstractConverter<Service> {
        @Autowired
        ServiceFacade facade;

        public ConverterByName() {
            super(Service.class);
        }

        @Override
        public AbstractFacade<Service> getFacade() {
            return facade;
        }
    }
}
