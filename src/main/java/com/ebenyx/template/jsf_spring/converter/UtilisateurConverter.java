package com.ebenyx.template.jsf_spring.converter;

import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.ActeurFacade;
import com.ebenyx.template.jsf_spring.model.Acteur;
import javax.faces.convert.FacesConverter;
import org.springframework.beans.factory.annotation.Autowired;

public class UtilisateurConverter{
    @FacesConverter(forClass = Acteur.class)
    public static class ConverterByClass extends AbstractConverter<Acteur> {
        @Autowired
        ActeurFacade facade;
        public ConverterByClass() {
            super(Acteur.class);
        }

        @Override
        public AbstractFacade<Acteur> getFacade() {
            return facade;
        }
    }    
    @FacesConverter(value ="UtilisateurConverter")
    public static class ConverterByName extends AbstractConverter<Acteur> {
        @Autowired
        ActeurFacade facade;

        public ConverterByName() {
            super(Acteur.class);
        }

        @Override
        public AbstractFacade<Acteur> getFacade() {
            return facade;
        }
    }
}
