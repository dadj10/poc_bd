package com.ebenyx.template.jsf_spring.converter;

import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.EvenementFacade;
import com.ebenyx.template.jsf_spring.model.Evenement;
import javax.faces.convert.FacesConverter;
import org.springframework.beans.factory.annotation.Autowired;

public class EvenementConverter{
    @FacesConverter(forClass = Evenement.class)
    public static class ConverterByClass extends AbstractConverter<Evenement> {
        @Autowired
        EvenementFacade facade;
        public ConverterByClass() {
            super(Evenement.class);
        }

        @Override
        public AbstractFacade<Evenement> getFacade() {
            return facade;
        }
    }    
    @FacesConverter(value ="EvenementConverter")
    public static class ConverterByName extends AbstractConverter<Evenement> {
        @Autowired
        EvenementFacade facade;

        public ConverterByName() {
            super(Evenement.class);
        }

        @Override
        public AbstractFacade<Evenement> getFacade() {
            return facade;
        }
    }
}
