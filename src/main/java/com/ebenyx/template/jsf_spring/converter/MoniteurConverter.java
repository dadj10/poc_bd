package com.ebenyx.template.jsf_spring.converter;

import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.MoniteurFacade;
import com.ebenyx.template.jsf_spring.model.Moniteur;
import javax.faces.convert.FacesConverter;
import org.springframework.beans.factory.annotation.Autowired;

public class MoniteurConverter{
    @FacesConverter(forClass = Moniteur.class)
    public static class ConverterByClass extends AbstractConverter<Moniteur> {
        @Autowired
        MoniteurFacade facade;
        public ConverterByClass() {
            super(Moniteur.class);
        }

        @Override
        public AbstractFacade<Moniteur> getFacade() {
            return facade;
        }
    }    
    @FacesConverter(value ="MoniteurConverter")
    public static class ConverterByName extends AbstractConverter<Moniteur> {
        @Autowired
        MoniteurFacade facade;

        public ConverterByName() {
            super(Moniteur.class);
        }

        @Override
        public AbstractFacade<Moniteur> getFacade() {
            return facade;
        }
    }
}
