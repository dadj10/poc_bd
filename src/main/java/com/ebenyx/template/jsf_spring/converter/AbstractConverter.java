/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.converter;

import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.model.BaseEntity;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public abstract class AbstractConverter<T extends BaseEntity> implements javax.faces.convert.Converter {

    private Class<T> entityClass;

    public abstract AbstractFacade<T> getFacade();

    protected AbstractConverter(Class<T> entityClass) {
        this.entityClass = entityClass;
        init();
    }

    private void init() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        ServletContext servletContext = (ServletContext) externalContext.getContext();
        WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext).
                getAutowireCapableBeanFactory().
                autowireBean(this);
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        try {
            if (value == null || value.length() == 0) {
                return null;
            }
            AbstractFacade facade = getFacade();
            return facade.builder().where("id", Long.parseLong(value)).findOneEntity();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
        try {
            if (object == null) {
                return null;
            }
            T o = entityClass.cast(object);
            return o.getId().toString();
        } catch (Exception ex) {
            System.err.println("conversion impossible : " + object);
        }

        return null;
    }
}
