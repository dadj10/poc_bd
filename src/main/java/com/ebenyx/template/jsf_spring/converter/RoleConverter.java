package com.ebenyx.template.jsf_spring.converter;

import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.RoleFacade;
import com.ebenyx.template.jsf_spring.model.Role;
import javax.faces.convert.FacesConverter;
import org.springframework.beans.factory.annotation.Autowired;

public class RoleConverter{
    @FacesConverter(forClass = Role.class)
    public static class ConverterByClass extends AbstractConverter<Role> {
        @Autowired
        RoleFacade facade;
        public ConverterByClass() {
            super(Role.class);
        }

        @Override
        public AbstractFacade<Role> getFacade() {
            return facade;
        }
    }    
    @FacesConverter(value ="RoleConverter")
    public static class ConverterByName extends AbstractConverter<Role> {
        @Autowired
        RoleFacade facade;

        public ConverterByName() {
            super(Role.class);
        }

        @Override
        public AbstractFacade<Role> getFacade() {
            return facade;
        }
    }
}
